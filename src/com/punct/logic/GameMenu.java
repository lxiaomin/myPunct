package com.punct.logic;

import com.punct.SQL.SQL;
import com.punct.bgm.BackgroundMusic;
import com.punct.bgm.ButtonBGM;
import com.punct.netmode.common.RoomSearch;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class GameMenu extends Application {
	public static BackgroundMusic bMusic;
	static int register_img_couner = 0;
	  static ButtonBGM m2;
	public static boolean flag=true;
	@FXML
	private Button btn_help;

	@FXML
	private Button btn_exit;

	@FXML
	private ImageView img_music_0;

	@FXML
	private Button btn_alone;

	@FXML
	private Button btn_combind;

	@FXML
	private ImageView img_music_fish;

	@FXML
	private Button btn_mechine;

	@FXML
	void alone(ActionEvent event) {
		Platform.runLater(new Runnable() {
			public void run() {
				try {
					new StandaloneBegin().start(new Stage());
				} catch (Exception e) {

					e.printStackTrace();
				}
			}

		});
		((javafx.scene.Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口

	}

	@FXML
	void combind(ActionEvent event) {
		Platform.runLater(new Runnable() {
			public void run() {
				try {

					new RoomSearch().start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		((javafx.scene.Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口

	}

	@FXML
	void mechine(ActionEvent event) {


	}

	@FXML
	void help(ActionEvent event) {
		Platform.runLater(new Runnable() {
			public void run() {
				try {
					new Role().start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});

	}

	@FXML
	void exit(ActionEvent event) {
		((javafx.scene.Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口

	}

	public GameMenu() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		launch(args);

	}

	EventHandler<MouseEvent>  MousemoveOn=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg2.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};
	EventHandler<MouseEvent>  MousemoveOFF=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};
	EventHandler<MouseEvent>  MousemoveOFF2=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg3.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};

	EventHandler<MouseEvent>  Musicclick=new EventHandler<MouseEvent>() {

		@SuppressWarnings("deprecation")
		@Override
		public void handle(MouseEvent arg0) {
			BackgroundMusic.musicThread.resume();

			img_music_0.setOnMouseClicked(MusicOFFclick);
			img_music_0.setOnMouseEntered(MousemoveOFF2);
			img_music_0.setOnMouseExited(MousemoveOFF);
			flag=true;
			Image image2 = new Image(getClass().getResource("../view/musicImg.png").toExternalForm());
			img_music_0.setImage(image2);
			Image image = new Image(getClass().getResource("../view/musicOn.png").toExternalForm());
			img_music_fish.setImage(image);
			rollImg();
		}
	};

	EventHandler<MouseEvent>  MusicOFFclick=new EventHandler<MouseEvent>() {

		@SuppressWarnings("deprecation")
		@Override
		public void handle(MouseEvent arg0) {
			BackgroundMusic.musicThread.suspend();
			img_music_0.setOnMouseClicked(Musicclick);
			img_music_0.setOnMouseEntered(MousemoveOFF);
			img_music_0.setOnMouseExited(MousemoveOn);
			flag=false;
			Image image2 = new Image(getClass().getResource("../view/musicImg3.png").toExternalForm());
			img_music_0.setImage(image2);
			Image image = new Image(getClass().getResource("../view/musicOff.png").toExternalForm());
			img_music_fish.setImage(image);
			rollImg();
		}
	};

	/**************************** 鼠标按钮特效事件 **************************/
	EventHandler<MouseEvent> Outeffect = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {

			javafx.scene.Node node = (javafx.scene.Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_alone")) {
				Image image = new Image(getClass().getResource("../view/aloneGame.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_alone.setEffect(replaceImageInput);
			}
			if (name.equals("btn_combind")) {
				Image image = new Image(getClass().getResource("../view/persontoperson.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_combind.setEffect(replaceImageInput);
			}
			if (name.equals("btn_exit")) {
				Image image = new Image(getClass().getResource("../view/exitGame.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_exit.setEffect(replaceImageInput);
			}
			if (name.equals("btn_help")) {
				Image image = new Image(getClass().getResource("../view/GameHelp.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_help.setEffect(replaceImageInput);
			}
			if (name.equals("btn_mechine")) {
				Image image = new Image(getClass().getResource("../view/personToCom.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_mechine.setEffect(replaceImageInput);
			}

		}
	};

	/**************************** 鼠标按钮特效事件 **************************/
	EventHandler<MouseEvent> Ineffect = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {

			javafx.scene.Node node = (javafx.scene.Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_alone")) {
				Choose("1");
				Image image = new Image(getClass().getResource("../view/aloneGame2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_alone.setEffect(replaceImageInput);
			}
			if (name.equals("btn_combind")) {
				Choose("2");
				Image image = new Image(getClass().getResource("../view/persontoperson2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_combind.setEffect(replaceImageInput);
			}
			if (name.equals("btn_exit")) {
				Choose("3");
				Image image = new Image(getClass().getResource("../view/exitGame2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_exit.setEffect(replaceImageInput);
			}
			if (name.equals("btn_help")) {
				Choose("4");
				Image image = new Image(getClass().getResource("../view/GameHelp2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_help.setEffect(replaceImageInput);
			}
			if (name.equals("btn_mechine")) {
				Choose("5");
				Image image = new Image(getClass().getResource("../view/personToCom2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				btn_mechine.setEffect(replaceImageInput);
			}

		}
	};
	private void rollImg() {

		new Thread() {
			int count=0;
			public void run() {
				while(flag)
				{
					count++;
					img_music_fish.setRotate(count%360);
					try {

						sleep(50);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	/*音乐*/
	private void Choose(String string) {
		new Thread() {
			public void run() {
				m2.PlayBubblesMusic(string);
			}
		}.start();
	}

	private void TextBoxBGM() {
		new Thread() {
			public void run() {
				m2.PlayTypeMusic("5");
			}
		}.start();
	}


	@FXML
	private void initialize() {

		btn_alone.setOnMouseEntered(Ineffect);
		btn_exit.setOnMouseEntered(Ineffect);
		btn_help.setOnMouseEntered(Ineffect);
		btn_combind.setOnMouseEntered(Ineffect);
		btn_mechine.setOnMouseEntered(Ineffect);

		btn_alone.setOnMouseExited(Outeffect);
		btn_exit.setOnMouseExited(Outeffect);
		btn_help.setOnMouseExited(Outeffect);
		btn_combind.setOnMouseExited(Outeffect);
		btn_mechine.setOnMouseExited(Outeffect);

		img_music_0.setOnMouseClicked(Musicclick);
		img_music_0.setOnMouseEntered(MousemoveOFF);
		img_music_0.setOnMouseExited(MousemoveOn);

	}
	@Override
	public void start(Stage stage) throws Exception {
		try {

			FXMLLoader loader = new FXMLLoader(UserLogin.class.getResource("../view/menu.fxml"));
<<<<<<< HEAD
			try {
				bMusic=new BackgroundMusic();
				bMusic.startPlay();
				BackgroundMusic.musicThread.suspend();
			} catch (Exception e) {
				System.out.println("无法播放音乐");
			}

=======
			bMusic=new BackgroundMusic();
			bMusic.startPlay();
			BackgroundMusic.musicThread.suspend();
>>>>>>> origin/鏇存敼鐜鍚庣殑鍒嗘敮
			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("游戏菜单");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.setOnCloseRequest(e -> System.exit(0));// 完全退出
			stage.show();
			m2=new ButtonBGM();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
