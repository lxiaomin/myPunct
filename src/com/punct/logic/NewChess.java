package com.punct.logic;

import com.punct.controlers.ArchBottom;
import com.punct.controlers.ArchCenter;
import com.punct.controlers.ArchTop;
import com.punct.controlers.LineCenter;
import com.punct.controlers.LineTop;
import com.punct.controlers.Triangle;

/*http://blog.csdn.net/kongxx/article/details/7259827*/
public class NewChess {

	private int color,NowchessDir,level,steps,NowChessType;
	private String NowChessID;
	int XYdata[]=new int[2];

	public NewChess(int color, int nowchessDir, int level, int steps, int nowChessType, String nowChessID,
			int[] xYdata) {
		super();
		this.color = color;
		NowchessDir = nowchessDir;
		this.level = level;
		this.steps = steps;
		NowChessType = nowChessType;
		NowChessID = nowChessID;
		XYdata = xYdata;
		addNewChess(nowChessType);
	}

	public void addNewChess(int nowChessType)
	{
		switch (nowChessType) {
		case 0: LineTop lineTop=new LineTop();
				setLineTop(lineTop,NowChessID+"_"+steps+"_"+level,level);

		break;
		case 1: LineCenter lineCenter=new LineCenter();
		setLineCenter(lineCenter,NowChessID+"_"+steps+"_"+level,level);
		break;
		case 2: Triangle triangle=new Triangle();
				setTriangle(triangle,NowChessID+"_"+steps+"_"+level,level);

		break;
		case 3:	ArchTop archTop=new ArchTop();
				setArchTop(archTop,NowChessID+"_"+steps+"_"+level,level);
		break;
		case 4:ArchCenter archCenter=new ArchCenter();
			setArchCenter(archCenter,NowChessID+"_"+steps+"_"+level,level);
		break;
		case 5:ArchBottom archBottom=new ArchBottom();
		setArchBottom(archBottom,NowChessID+"_"+steps+"_"+level,level);

		break;
		default:
			break;
		};
	}
	public void setLineCenter(LineCenter lineCenter, String ChessName,int thisLevel) {

		lineCenter.setColor(color);
		lineCenter.setDir(NowchessDir);
		lineCenter.setLevel(level);
		lineCenter.setOrder(steps);
		lineCenter.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		lineCenter.center.setDatax(XYdata[0]);
		lineCenter.center.setDatay(XYdata[1]);
		lineCenter.center.dir=NowchessDir;
		lineCenter.center.chessName=ChessName;
		lineCenter.center.color=color;
		lineCenter.center.index=steps;
		lineCenter.center.level=level;
		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		lineCenter.top.color=color;
		lineCenter.buttom.color=color;
		switch (NowchessDir)
		{
		case 0:CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = lineCenter.buttom ;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = lineCenter.top;
		break;
		case 1:
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;
		if(XYdata[0]<8)
		{
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = lineCenter.buttom ;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = lineCenter.top;
		}
		else if(XYdata[0]==8)
		{
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = lineCenter.buttom;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;

		}
		else
		{
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;

		}
		break;
		case 2:
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;
			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = lineCenter.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = lineCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = lineCenter.top;

			}
			break;
		default:break;
		}

	}
	/*设置棋子的初始化条件。等待加入棋盘模型中*/
	protected  void setLineTop(LineTop lineTop,String ChessName,int thisLevel) {

		lineTop.setColor(color);
		lineTop.setDir(NowchessDir);
		lineTop.setLevel(level);
		lineTop.setOrder(steps);
		lineTop.top.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		lineTop.top.setDatax(XYdata[0]);
		lineTop.top.setDatay(XYdata[1]);
		lineTop.top.dir=NowchessDir;
		lineTop.top.chessName=ChessName;
		lineTop.top.color=color;
		lineTop.top.index=steps;
		lineTop.top.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		lineTop.buttom.color=color;
		lineTop.center.color=color;

		switch (NowchessDir) {
		case 0://0是竖直向下的，然后剩下的12345是顺时针方向的几个剩下的方向
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=	lineTop.top;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode=lineTop.center;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-2].chessNode=lineTop.buttom;
//			OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);
//			OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode);
//			OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-2].chessNode);
			break;
		case 1:
			if(XYdata[0]<9)	{CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-2].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-2].chessNode);}
			else if(XYdata[0]==9) {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode);}

			else {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode);}

			break;
		case 2:
			if(XYdata[0]<9)	{CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode);}

			else if(XYdata[0]==9) {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode);}

			else {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+2].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+2].chessNode);}

			break;
		case 3:
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode=lineTop.center;
			CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+2].chessNode=lineTop.buttom;
			//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);
			//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode);
			//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+2].chessNode);
			break;
		case 4:
			if(XYdata[0]<7)	{CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+2].chessNode=lineTop.buttom;}
			else if(XYdata[0]==7) {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode);}

			else {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode);}
			break;
		case 5:
			if(XYdata[0]<7)	{CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode);}

			else if(XYdata[0]==7) {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode);}

			else {CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode=lineTop.center;CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-2].chessNode=lineTop.buttom;
			}//OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode);OldChessList.add(CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-2].chessNode);}
			break;
		default:
			break;

		}



	}

	protected  void setArchBottom(ArchBottom archBottom, String ChessName,int thisLevel) {
		archBottom.setColor(color);
		archBottom.setDir(NowchessDir);
		archBottom.setLevel(level);
		archBottom.setOrder(steps);
		archBottom.buttom.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archBottom.buttom.setDatax(XYdata[0]);
		archBottom.buttom.setDatay(XYdata[1]);
		archBottom.buttom.dir=NowchessDir;
		archBottom.buttom.chessName=ChessName;
		archBottom.buttom.color=color;
		archBottom.buttom.index=steps;
		archBottom.buttom.level=level;
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archBottom.buttom;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archBottom.top.color=color;
		archBottom.center.color=color;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+2].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =archBottom.top;

			}
			break;
		case 1:
			if(XYdata[0]<7)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==7)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode = archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode = archBottom.top;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode = archBottom.top;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode = archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archBottom.top;
			}
			break;
		case 4:
			if(XYdata[0]<=8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode =archBottom.top;

			}
			else if(XYdata[0]==9)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode =archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode =archBottom.top;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.center;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+2].chessNode = archBottom.top;

			}
			break;
		default:
			break;
		}

	}

	protected  void setArchCenter(ArchCenter archCenter, String ChessName,int thisLevel) {
		archCenter.setColor(color);
		archCenter.setDir(NowchessDir);
		archCenter.setLevel(level);
		archCenter.setOrder(steps);
		archCenter.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archCenter.center.setDatax(XYdata[0]);
		archCenter.center.setDatay(XYdata[1]);
		archCenter.center.dir=NowchessDir;
		archCenter.center.chessName=ChessName;
		archCenter.center.color=color;
		archCenter.center.index=steps;
		archCenter.center.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archCenter.buttom.color=color;
		archCenter.top.color=color;

		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archCenter.center;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}

			break;
		case 1:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.top;
			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archCenter.buttom;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;

			}
			break;
		case 4:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode =  archCenter.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode =  archCenter.buttom;
			}
			break;

		case 5:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.buttom;

			}
			break;

		default:
			break;
		}

	}

	protected  void setArchTop(ArchTop archTop, String ChessName,int thisLevel) {
		archTop.setColor(color);
		archTop.setDir(NowchessDir);
		archTop.setLevel(level);
		archTop.setOrder(steps);
		archTop.top.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archTop.top.setDatax(XYdata[0]);
		archTop.top.setDatay(XYdata[1]);
		archTop.top.dir=NowchessDir;
		archTop.top.chessName=ChessName;
		archTop.top.color=color;
		archTop.top.index=steps;
		archTop.top.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archTop.buttom.color=color;
		archTop.center.color=color;


		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archTop.top;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			break;
		case 1:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
					CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode =  archTop.buttom;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+2].chessNode =  archTop.buttom;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+2].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			break;
		case 4:
			if(XYdata[0]<7)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==7)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode =  archTop.buttom;

			}

			break;
		default:
			break;
		}


	}

	protected  void setTriangle(Triangle triangle, String ChessName,int thisLevel) {
		triangle.setColor(color);
		triangle.setDir(NowchessDir);
		triangle.setLevel(level);
		triangle.setOrder(steps);
		triangle.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		triangle.center.setDatax(XYdata[0]);
		triangle.center.setDatay(XYdata[1]);
		triangle.center.dir=NowchessDir;
		triangle.center.chessName=ChessName;
		triangle.center.color=color;
		triangle.center.index=steps;
		triangle.center.level=level;


		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		triangle.left.color=color;
		triangle.right.color=color;

		CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=triangle.center;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left ;
						CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.right;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left ;
						CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.right;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = triangle.left ;
						CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			break;
		case 1:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = triangle.right;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode  = triangle.left;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode =triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.left;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.left;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.left;

			}
			break;
		case 4:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.right ;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.right ;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.left;

			}
			else
			{
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				CommonChessBoard.chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left;

			}
			break;
		default:
			break;
		}

	}


}
