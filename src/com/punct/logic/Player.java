package com.punct.logic;

public class Player {

	private String name,ip;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Player() {
		// TODO Auto-generated constructor stub
	}

	public Player(String name, String ip, int id) {
		super();
		this.name = name;
		this.ip = ip;
		this.id = id;
	}


}
