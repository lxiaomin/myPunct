package com.punct.logic;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Verify {

	public Verify(){;}

	/*验证邮箱格式*/
	public boolean checkEmail(String email)
	{

		boolean flag = false;
	    String check ="\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"; //定义正则表达式
	    Pattern regex =Pattern.compile(check); //   将给定的正则表达式编译到模式中。
	    Matcher  matcher= regex.matcher(email);//   创建匹配给定输入与此模式的匹配器。
	    try
	    {
	    	flag = matcher.matches();//判断匹配与否，赋值给flag
	    }
	   catch(Exception e){
		    flag = false;
		   }
		  return flag;

	}
	public String checkUID(String uid){  //检查UID是否符合规则

		if(uid.length()>=16)
		{
			return "用户名不能长于16位";
		}



		return "";

	}

	public int checkPSW(String psw){  //检查密码是否符合规则

		int flag=0;
		if(psw.length()>=16)
		{
			flag= 1;
		}
		else if(!psw.equals("") && psw.length()<=6)
		{
			flag= 2;
		}
		else
		{
			    String check = "[\u4e00-\u9fa5]"; //定义正则表达式
			    Pattern regex =Pattern.compile(check); //   将给定的正则表达式编译到模式中。
			    Matcher  matcher= regex.matcher(psw);//   创建匹配给定输入与此模式的匹配器。
			    try
			    {
			    	if(matcher.find())
			    	{
			    		flag=3;
			    	}
			    	else
			    	{
			    		return flag;
			    	}
			    }
			   catch(Exception e){
				   flag= 4;
				}
		}
		return flag;

	}


}
