package com.punct.logic;

import com.punct.netmode.common.Message;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WaitingResult  extends Application {
    @FXML
    private Button cancelButton;

    @FXML
    private Label detailsLabel;

    @FXML
    private HBox actionParent;

    @FXML
    private Button okButton;

    @FXML
    private HBox okParent;

    @FXML
    private Label messageLabel;

    @FXML
    void cloce(ActionEvent event) {
    	((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏
    }

    @FXML
    void waitting(ActionEvent event) {

    }
	public WaitingResult() {

	}

	public static void main(String[] args) {
		launch(args);

	}


	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Role.class.getResource("../view/waitting.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("等待对方同意");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间

			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.show();
			/*发送请求*/

			Message regretMessage=new Message(2,true);
			if(CommonChessBoard.IsServer)
				CommonChessBoard.getThisserver().sendMessage(regretMessage);
			else
				CommonChessBoard.getThisclient().sendMessage(regretMessage);



		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
