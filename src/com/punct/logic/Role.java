package com.punct.logic;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Role extends Application {
	private static int page=1;

    @FXML
    private ImageView img_bg;

    @FXML
    private Button btn_nextPage;

    @FXML
    private Button btn_lastPage;

    @FXML
    private ImageView img_pages;
	public Role() {



	}

	/**************************** 鼠标滑过登陆按钮特效事件 **************************/
	EventHandler<MouseEvent> add = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {
			page++;
			switch (page) {
			case 1:
				btn_lastPage.setVisible(false);
				btn_nextPage.setVisible(true);
				Image image=new Image(getClass().getResource("../view/page1.png").toExternalForm());
				img_bg.setImage(image);
				Image image2=new Image(getClass().getResource("../view/page1.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			case 2:
				btn_lastPage.setVisible(true);
				btn_nextPage.setVisible(true);
				 image=new Image(getClass().getResource("../view/role2.png").toExternalForm());
				img_bg.setImage(image);
				 image2=new Image(getClass().getResource("../view/page2.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			case 3:
				btn_lastPage.setVisible(true);
				btn_nextPage.setVisible(false);
				 image=new Image(getClass().getResource("../view/role3.png").toExternalForm());
				img_bg.setImage(image);
				 image2=new Image(getClass().getResource("../view/page3.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			default:
				break;
			}



		}
	};

	/**************************** 鼠标滑过登陆按钮特效事件 **************************/
	EventHandler<MouseEvent> sub = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {
			page--;
			switch (page) {
			case 1:
				btn_lastPage.setVisible(false);
				btn_nextPage.setVisible(true);
				Image image=new Image(getClass().getResource("../view/role1.png").toExternalForm());
				img_bg.setImage(image);
				Image image2=new Image(getClass().getResource("../view/page1.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			case 2:
				btn_lastPage.setVisible(true);
				btn_nextPage.setVisible(true);
				 image=new Image(getClass().getResource("../view/role2.png").toExternalForm());
				img_bg.setImage(image);
				 image2=new Image(getClass().getResource("../view/page2.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			case 3:
				btn_lastPage.setVisible(true);
				btn_nextPage.setVisible(false);
				 image=new Image(getClass().getResource("../view/role3.png").toExternalForm());
				img_bg.setImage(image);
				 image2=new Image(getClass().getResource("../view/page3.png").toExternalForm());
				img_pages.setImage(image2);
				break;
			default:
				break;
			}



		}
	};

	@FXML
	private void initialize() {
		btn_lastPage.setOnMouseClicked(sub);
		btn_nextPage.setOnMouseClicked(add);
		btn_lastPage.setVisible(false);
	}


	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Role.class.getResource("../view/Role.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("游戏规则");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间

			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
