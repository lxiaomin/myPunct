package com.punct.logic;

import com.punct.netmode.common.Message;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Regret extends Application {

	public Regret() {

	}

	@FXML
    private Button cancelButton;

    @FXML
    private Label detailsLabel;

    @FXML
    private HBox actionParent;

    @FXML
    private Button okButton;

    @FXML
    private HBox okParent;

    @FXML
    private Label messageLabel;

    @FXML
    void cancelEvent(ActionEvent event) {

    	Message agreeMessage=new Message(3,false);
    	if(CommonChessBoard.IsServer) CommonChessBoard.getThisserver().sendMessage(agreeMessage);
		else CommonChessBoard.getThisclient().sendMessage(agreeMessage);
    	((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏
    }
	public static void main(String[] args) {
		launch(args);

	}
    @FXML
    void OKevent(ActionEvent event) {

    	CommonChessBoard.regretItself();
    	Message agreeMessage=new Message(3,true);
    	if(CommonChessBoard.IsServer) CommonChessBoard.getThisserver().sendMessage(agreeMessage);
		else CommonChessBoard.getThisclient().sendMessage(agreeMessage);

    	((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏
    }

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Role.class.getResource("../view/regret.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("悔棋请求");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间

			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
