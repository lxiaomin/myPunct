package com.punct.logic;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Points extends Application {


    @FXML
    private TableColumn<?, ?> playerName;

    @FXML
    private TableColumn<?, ?> PlayerPoints;

    @FXML
    private TableColumn<?, ?> Win;

    @FXML
    private TableView<?> tab_paihang;

	public Points() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		launch(args);

	}


	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(UserLogin.class.getResource("../view/points.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("玩家积分");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.setOnCloseRequest(e -> System.exit(0));// 完全退出
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
