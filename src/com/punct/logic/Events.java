package com.punct.logic;

import com.punct.ModelDetective.WINJUDGE;
import com.punct.ModelDetective.WinLine;
import com.punct.bgm.BackgroundMusic;
import com.punct.bgm.ButtonBGM;
import com.punct.controlers.ArchBottom;
import com.punct.controlers.ArchCenter;
import com.punct.controlers.ArchTop;
import com.punct.controlers.ChessBoard;
import com.punct.controlers.ChessBoardModel;
import com.punct.controlers.ChessPoints;
import com.punct.controlers.ColorControler;
import com.punct.controlers.LineCenter;
import com.punct.controlers.LineTop;
import com.punct.controlers.Triangle;
import com.punct.move.AppendChessImg;
import com.punct.move.ChessMove;
import com.punct.move.ChessMoveToNullPositionProvider;
import com.punct.move.ChessMoveToUpLevelPositionProvider;
import com.punct.move.ChessMoveToUpLevelRotateDirProvider;
import com.punct.move.ChessPointsProvider;
import com.punct.move.ChessRotatePositionProvider;
import com.punct.move.ClearPoints;
import com.punct.move.JudgeIsCovered;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Events {

	public Events() {

	}


	/**************************** 按钮特效1 **************************/
	 EventHandler<MouseEvent> moveEffectEventHandler =new EventHandler<MouseEvent>() {


		@Override
		public void handle(MouseEvent event) {
			SelectOptionBGM();
			ronateWhile=true;
			javafx.scene.Node node = (javafx.scene.Node) event.getSource();
			String name = node.getId();
			if (name.equals("RotateChess")) {
				Image image = new Image(getClass().getResource("../view/xuanzhuan2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RotateChess.setEffect(replaceImageInput);
				Image image2 = new Image(getClass().getResource("../view/red.png").toExternalForm());
				img_rotate1.setImage(image2);
				redRoll(1);
			}
			if (name.equals("RestartMyTurn")) {
				Image image = new Image(getClass().getResource("../view/chongxia2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RestartMyTurn.setEffect(replaceImageInput);
				Image image2 = new Image(getClass().getResource("../view/red.png").toExternalForm());
				img_rotate2.setImage(image2);
				redRoll(2);
			}
			if (name.equals("Comfirm")) {
				Image image = new Image(getClass().getResource("../view/fangzhi2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				Comfirm.setEffect(replaceImageInput);
				Image image2 = new Image(getClass().getResource("../view/red.png").toExternalForm());
				img_rotate3.setImage(image2);
				redRoll(3);
			}
			if (name.equals("RegretChess")) {
				Image image = new Image(getClass().getResource("../view/huiqi2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RegretChess.setEffect(replaceImageInput);
				Image image2 = new Image(getClass().getResource("../view/red.png").toExternalForm());
				img_rotate4.setImage(image2);
				redRoll(4);
			}
			if (name.equals("GiveUp")) {
				Image image = new Image(getClass().getResource("../view/fangqi2.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				GiveUp.setEffect(replaceImageInput);
				Image image2 = new Image(getClass().getResource("../view/red.png").toExternalForm());
				img_rotate5.setImage(image2);
				redRoll(5);
			}


		}

		private void redRoll(int i) {
//
//			if(musicflag2){new Thread() {
//				double count=0;
//				@SuppressWarnings("deprecation")
//				public void run() {
//					try {
//
//							for(count=0;ronateWhile && count <3.6;count+=0.01)
//							{
//
//								if(count>3.6)	{this.interrupt();this.stop();}
//								root.lookup("#img_rotate"+i).setRotate((count*100)%360);
//								try {
//									sleep(10);
//									if(!ronateWhile) {this.interrupt();this.stop();}
//
//								} catch (InterruptedException e) {
//									this.interrupt();this.stop();
//									e.printStackTrace();
//								}
//							}
//
//					} catch (Exception e) {
//						this.interrupt();this.stop();
//					}
//
//				}
//			}.start();
//		}
		}
	};

	/**************************** 鼠标事件 **************************/
	 EventHandler<MouseEvent> moveoutEffectEventHandler =new EventHandler<MouseEvent>() {


		@Override
		public void handle(MouseEvent event) {
			 ronateWhile=false;
			javafx.scene.Node node = (javafx.scene.Node) event.getSource();
			String name = node.getId();
			if (name.equals("RotateChess")) {
				Image image = new Image(getClass().getResource("../view/xuanzhuan.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RotateChess.setEffect(replaceImageInput);
				img_rotate1.setImage(null);

			}
			if (name.equals("RestartMyTurn")) {
				Image image = new Image(getClass().getResource("../view/重下.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RestartMyTurn.setEffect(replaceImageInput);

				img_rotate2.setImage(null);

			}
			if (name.equals("Comfirm")) {
				Image image = new Image(getClass().getResource("../view/fangzhi.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				Comfirm.setEffect(replaceImageInput);

				img_rotate3.setImage(null);

			}
			if (name.equals("RegretChess")) {
				Image image = new Image(getClass().getResource("../view/huiqi.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				RegretChess.setEffect(replaceImageInput);

				img_rotate4.setImage(null);

			}
			if (name.equals("GiveUp")) {
				Image image = new Image(getClass().getResource("../view/fangqi.png").toExternalForm());
				ImageInput replaceImageInput = new ImageInput();
				replaceImageInput.setSource(image);
				GiveUp.setEffect(replaceImageInput);

				img_rotate5.setImage(null);

			}


		}


	};

	/**************************** 鼠标点击选择棋子事件 **************************/
	static EventHandler<MouseEvent> clickChess=new EventHandler<MouseEvent>() {

		/*点击下棋的时候步骤：*/
		@Override
		public void handle(MouseEvent event) {
			SelectChessBGM();
			LoseBindCMoveEvent();
			InitChessPointsInit();
			/*把刚刚给你选择移动棋子的点都灭掉*/
			setEnableNullMoveChess();
			Pane pane=(Pane)event.getSource();
			pane.setStyle("-fx-border-style:solid;-fx-border-radius:10;-fx-border-color:red;-fx-border-width:4;");
			pane.setOnMouseExited(null);
			for(int i=0;i<6;i++)
			{
				if(color==1)
				{
					String paneNameString="pane_white_"+i;
					Node node=root.lookup("#"+paneNameString+"");
					if(paneNameString.equals(pane.getId()))
					{
						NowChessID=pane.getId();
						NowChessType=i;
						String atempString=ChessPointsProvider.getAvailableChessPoints(i,0);//获取可用的下棋点
						String availablePoints[]=atempString.split(",");
						EnableAvailablePoints(availablePoints);
						AtiveChessPoints=availablePoints;
					}
					else
					{
						node.setDisable(true);
						node.setStyle("");
					}
				}
				else
				{
					String paneNameString="pane_black_"+i;
					Node node=root.lookup("#"+paneNameString+"");
					if(paneNameString.equals(pane.getId()))
					{
						NowChessID=pane.getId();
						NowChessType=i;
						String atempString=ChessPointsProvider.getAvailableChessPoints(i,0);//获取可用的下棋点
						String availablePoints[]=atempString.split(",");
						EnableAvailablePoints(availablePoints);
						AtiveChessPoints=availablePoints;
					}
					else
					{
						node.setDisable(true);
						node.setStyle("");
					}
				}
			}

			/*设置需要取消的类型是下了新的棋子*/
			CancalType=0;
			ManagedButtons();//管理按钮
			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮

		}

		private void setEnableNullMoveChess() {
			for(int i=0;i<OldChessListIndex;i++)
			{
				ChessPoints cP=(ChessPoints)OldChessList.get(i);

				ImageView pointImageView=(ImageView)root.lookup("#p_"+cP.getDatax()+"_"+cP.getDatay());
			//	pointImageView.setOnMouseClicked(moveChessOnClick);
				pointImageView.setVisible(false);

			}

		}



	};
	/**************************** 鼠标点击选择棋子事件 **************************/

	/**************************** 核心事件处理1：落子点增加棋子图片 **************************/
	static EventHandler<MouseEvent> clickPoints=new EventHandler<MouseEvent>() {

		/*点击下棋的时候：*/
		@Override
		public void handle(MouseEvent event) {
			PlayChessBGM();
			/*获取被点击的落子点*/
			ImageView iView=(ImageView) event.getSource();

			/*必须保存的一下属性*/
			String data[] =iView.getId().split("_");
			XYdata[0]=Integer.valueOf(data[1]);
			XYdata[1]=Integer.valueOf(data[2]);
			XYLocation[0]=Integer.valueOf((int) iView.getLayoutX());
			XYLocation[1]=Integer.valueOf((int) iView.getLayoutY());
			XYRelative[0]=Integer.valueOf((int) iView.getX());
			XYRelative[0]=Integer.valueOf((int) iView.getY());
			/*获取能放置的方向*/
			//System.out.println("else if(i=="+XYdata[0]+" && j=="+XYdata[1]+") return false;");
			int dir =fitableChessPositionProvider(NowchessDir);
			System.out.println("现在的方向是"+dir);
			/*开始增加图片，设置新棋子在棋盘中的坐标*/
			int x=AppendChessImg.getChessLocationX(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),dir);
			int y=AppendChessImg.getChessLocationY(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),dir);
			ImageView newChessImg=new ImageView();
			String chessImgURL=AppendChessImg.getChessImgURL(NowChessID);
			newChessImg.setImage(new Image(getClass().getResource("../view/"+chessImgURL).toExternalForm()));
			newChessImg.setLayoutX(x);
			newChessImg.setLayoutY(y);
			newChessImg.setVisible(true);
			newChessImg.setId("tempChessImg");
			//根据判断函数传送回来的合适的方向进行旋转构造方法： Rotate(double angle, double pivotX, double pivotY) 参数是角度，以那个点为旋转轴心
			newChessImg.getTransforms().add(new Rotate(getRotateValue(0,dir),AppendChessImg.getSuitableRotateX(NowChessType, iView.getX()),AppendChessImg.getSuitableRotateY(NowChessType, iView.getY())));
			Pane temp=(Pane)root.lookup("#chessArea");
			temp.getChildren().add(newChessImg);
			/*增加图片成功，隐藏其它的下棋点*/
			diableAvilablePoints(AtiveChessPoints);
			/*堵塞现在颜色的棋子*/
			disableAllColor();
			/*棋子数量显示-1,但是没有保存*/
			String chessNum[]=NowChessID.split("_");
			Label counter_laber=(Label)root.lookup("#lab_"+chessNum[1]+"_counter_"+chessNum[2]);
			String aString=String.valueOf(Integer.valueOf(counter_laber.getText())-1);
			counter_laber.setText(aString);
			/*zanshi*/
			NowchessDir=dir;

			/*移动棋子并且已经走了*/
			CancalType=1;ManagedButtons();
			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮
		}



	};

	/**************************** 核心事件处理2：用户确定后修改棋盘模型,保存数据***********************/
	static EventHandler<MouseEvent> userConClickEvent=new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

			for(int i=0;i<6;i++)
			{
				if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
				{
					/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
					Pane pane=(Pane)root.lookup("#"+NowChessID);
					pane.setOnMouseExited(PaneBorderOutEffect);

					switch (i) {
					case 0: LineTop lineTop=new LineTop();
							setLineTop(lineTop,NowChessID+"_"+steps+"_"+level,0);
					break;
					case 1: LineCenter lineCenter=new LineCenter();
							setLineCenter(lineCenter,NowChessID+"_"+steps+"_"+level,0);
					break;
					case 2: Triangle triangle=new Triangle();
							setTriangle(triangle,NowChessID+"_"+steps+"_"+level,0);

					break;
					case 3:	ArchTop archTop=new ArchTop();
							setArchTop(archTop,NowChessID+"_"+steps+"_"+level,0);
					break;
					case 4:ArchCenter archCenter=new ArchCenter();
						setArchCenter(archCenter,NowChessID+"_"+steps+"_"+level,0);
					break;
					case 5:ArchBottom archBottom=new ArchBottom();
					setArchBottom(archBottom,NowChessID+"_"+steps+"_"+level,0);

					break;
					default:
						break;
					};
				}
			}

			SaveAddDataTOSQL(NowChessID,NowChessID+"_"+steps+"_"+level,XYdata,NowchessDir,color,level,steps,NowChessType);//数据保存到数据库中

			modelStack.push(chessBoardModel);
			stepChessName.push(NowChessID+"_"+steps+"_"+level);
			ImageView tempV=(ImageView)root.lookup("#tempChessImg");
			tempV.setId(NowChessID+"_"+steps+"_"+level);
			steps++;
			/*反转角色*/
			ativeAnother();
			color=ColorControler.reverseColor(color);
			rotateProvoder=0;

			moveSwitch=0;
			/*激活可以对方可以移动的棋子*/
			setAtiveMoveActionChessPoints();

			/*修改完数据之后，设置level为0*/
			level=0;
			ChessBoardModel.getWhiteChessBoardModel();

			judgeWin(WINJUDGE.WhoWin());

			/*初始化取消事件*/
			CancalType=-1;
			ManagedButtons();
			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮
		}

		private void judgeWin(int whoWin) {
			if(whoWin==0)  return;
		    if(whoWin>0)
			{

		    	javafx.scene.canvas.Canvas canvas=WinLine.drowWineLine();
		    	canvas.setTranslateX(390);
		    	canvas.setTranslateY(25);
		    	root.getChildren().add(canvas);

				System.out.println("白色赢了");
			}
			if(whoWin<0) {
				javafx.scene.canvas.Canvas canvas=WinLine.drowWineLine();
		    	canvas.setTranslateX(390);
		    	canvas.setTranslateY(25);
		    	root.getChildren().add(canvas);

				System.out.println("黑色赢了");
			}

		}
	};

	/**************************** 核心事件处理3-1：自动匹配旋转棋子的位置***********************/

	static EventHandler<MouseEvent> rotateConClickEvent=new EventHandler<MouseEvent>(){
		@Override
		public void handle(MouseEvent event) {
			RonateChessBGM();
			ImageView newChessImg=(ImageView)root.lookup("#tempChessImg");
			//NowchessDir=fitableChessPositionProvider();
			int dir=fitableChessPositionProvider(NowchessDir);

			newChessImg.getTransforms().add(new Rotate(getRotateValue(NowchessDir,dir),AppendChessImg.getSuitableRotateX(NowChessType,XYRelative[0]),AppendChessImg.getSuitableRotateY(NowChessType, XYRelative[1])));
			NowchessDir=dir;
		}
	};
	/**************************** 核心事件处理3-2：自动匹配旋转棋子的位置,基于上层的旋转***********************/

	static EventHandler<MouseEvent> rotateConClickEvent2=new EventHandler<MouseEvent>(){


		@Override
		public void handle(MouseEvent event) {
			RonateChessBGM();
			int dir=getUpLevelDiration(level-1,NowChessType);
			ImageView newChessImg=(ImageView)root.lookup("#tempChessImg");

			newChessImg.getTransforms().add(new Rotate(getRotateValue(NowchessDir,dir),AppendChessImg.getSuitableRotateX(NowChessType,XYRelative[0]),AppendChessImg.getSuitableRotateY(NowChessType, XYRelative[1])));
			NowchessDir=dir;
		}

	};
	/**************************** 鼠标滑过选中边框事件 **************************/
	static EventHandler<MouseEvent> PaneBorderInEffect = new EventHandler<MouseEvent>() {
		// 事件管理器
		@Override
		public void handle(MouseEvent event) {
//			Node node = root.lookup("#pane_white_2");
//			int i=0;
//				System.out.println(node);
			Pane pane=(Pane)event.getSource();
			pane.setStyle("-fx-border-style:solid;-fx-border-radius:10;-fx-border-color:red;-fx-border-width:4;"); // 设置css样式
		}
	};
	/**************************** (确定这一步就是移动棋子了)核心事件4：移动棋子操作前，高亮可移动的落子点。并绑定等下可以移动棋子的核心事件5 *******/
	static EventHandler<MouseEvent> moveChessPreClick=new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			getBindCMoveEvent();
			/*获得确定按钮绑定事件*/
			//
			getBindCMoveEvent();
			 /*初始化旋转事件默认为false*/
			 isUpNeedRotate=false;
			/*获取被点击的落子点*/
			ImageView iView=(ImageView) event.getSource();
			String data[] =iView.getId().split("_");
			/*获取在该点上的棋子的名字id*/
			int x=Integer.valueOf(data[1]),y=Integer.valueOf(data[2]);
			oldDataXY[0]=x;oldDataXY[1]=y;
			/*取得层数*/
			if(chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode!=null)
				if(!chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.chessName.equals("")){
					nodeNameString=chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.chessName;
					oldChessName=nodeNameString;}
			if(chessBoardModel.chessBoard.chessPoints[level+1][x][y].chessNode!=null)
				if(!chessBoardModel.chessBoard.chessPoints[level+1][x][y].chessNode.chessName.equals(""))
					{nodeNameString=chessBoardModel.chessBoard.chessPoints[level+1][x][y].chessNode.chessName;	oldChessName=nodeNameString;}
			//level=chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.level;
			NowChessID=ChessMove.getMoveChessRealName(nodeNameString);
			//在选择要移动的棋子的时候也把棋子的层数拿出来
			level=ChessMove.getLevel(nodeNameString);
			oldLevel=level;
			//System.out.println("层数是："+level);
			/*将要移动的棋子的图片透明化*/
			root.lookup("#"+nodeNameString).setOpacity(0.6);
			NowChessType=chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.type;
			OldchessDir=chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.dir;
			NowchessDir=chessBoardModel.chessBoard.chessPoints[level][x][y].chessNode.dir;
			//OldChessListIndex=chessBoardModel.chessBoard.chessPoints[x][y].chessNode.index;
			//清除原来的点
			/*设置要清除的方向是怎么样的*/
			ClearPoints.ClearTempDir=NowchessDir;
			/*清理棋子引用的空间,在清空的时候已经有保存，可以撤销*/
			ClearPoints.level=level;
			ClearPoints.chessType=NowChessType;
			ClearPoints.clear(ClearPoints.ClearTempDir, oldDataXY);
			/*在高亮点击可移动点之前，不能高亮其他本玩家不能移动的棋子了，否则一次性可能可以移动两个棋子*/
			setEnableNullMoveChess();
			/*高亮点击可移动点之后的竖直方向的点,并绑定另一个移动棋子的事件*/
			for(int j=0;j<chessBoardModel.chessBoard.chessPoints[level][x].length;j++){
				if(judge(x,j)) setAtive(level,x,j);
			}
			/*高亮点击可1 4方向的点*/
			if(x<=8)
			{
				/*高亮点击可1 4方向的点*/
				int j=y+(8-x);
				for(int i=8;i>=0 && j>=0;i--,j--){if(judge(i,j)) setAtive(level,i,j);}
				j=y+(8-x);
				for(int i=9;i<17 && j<chessBoardModel.chessBoard.chessPoints[level][i].length;i++){if(judge(i,j)) setAtive(level,i,j);}
				/*高亮点击可2 5方向的点*/
				j=y;
				for(int i=8;i>=0 && j<chessBoardModel.chessBoard.chessPoints[level][i].length;i--){if(judge(i,j)) setAtive(level,i,j);}
				j=y-1;
				for(int i=9;i<17 && j>=0;i++,j--){if(judge(i,j)) setAtive(level,i,j);}
			}
			else
			{
				/*高亮点击可1 4方向的点*/
				int j=y;
				for(int i=8;i<17 && j<chessBoardModel.chessBoard.chessPoints[level][i].length;i++){if(judge(i,j)) setAtive(level,i,j);}
			    j=y;
				for(int i=8;i>=0 && j>=0;i--,j--){if(judge(i,j)) setAtive(level,i,j);}
				/*高亮点击可2 5方向的点*/
			    j=y+(x-8);
				for(int i=8;i<17 && j>=0;i++,j--){if(judge(i,j)) setAtive(level,i,j);}
				j=y+(x-8);
				for(int i=8;i>=0 && j<chessBoardModel.chessBoard.chessPoints[level][i].length;i--){if(judge(i,j)) setAtive(level,i,j);}

			}



			disableAllColor();
			/*想移动棋子但是还没走*/
			CancalType=2;ManagedButtons();

			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮

		}

		private boolean judge(int i, int j) {

			if(i==8 && j==0) return false;
			else if(i==8 && j==16)return false;
			else if(i==0 && j==0)return false;
			else if(i==0 && j==8)return false;
			else if(i==16 && j==0)return false;
			else if(i==16 && j==8)return false;
		 return true;
		}

		private void setEnableNullMoveChess() {
			for(int i=0;i<OldChessListIndex;i++)
			{
				ChessPoints cP=(ChessPoints)OldChessList.get(i);
				if(cP.getDatax()==oldDataXY[0] && cP.getDatay()==oldDataXY[1]){setAtive(level,cP.getDatax(),cP.getDatay());}
				else {
					ImageView pointImageView=(ImageView)root.lookup("#p_"+cP.getDatax()+"_"+cP.getDatay());
					pointImageView.setOnMouseClicked(moveChessOnClick);
					pointImageView.setVisible(false);
				}
			}

		}

		private void setAtive(int thislevel,int i,int j) {
				if((chessBoardModel.chessBoard.chessPoints[thislevel][i][j].chessNode==null) && ChessMoveToNullPositionProvider.IsPositionAvilable(thislevel, i, j, NowChessType))
				{

					ImageView pointImageView=(ImageView)root.lookup("#p_"+i+"_"+j);
					Pane areaPane=(Pane)root.lookup("#chessArea");
					areaPane.getChildren().remove(pointImageView);
					pointImageView.setOnMouseClicked(moveChessOnClick);
					pointImageView.setVisible(true);
					areaPane.getChildren().add(pointImageView);
				}
				else if(chessBoardModel.chessBoard.chessPoints[thislevel][i][j].chessNode!=null && (chessBoardModel.chessBoard.chessPoints[thislevel][i][j].chessNode.chessName=="") && chessBoardModel.chessBoard.chessPoints[thislevel][i][j].chessNode.color==color)
				{
					if(ChessMoveToUpLevelPositionProvider.IsPositionAvilable(thislevel, i, j,NowChessType))
					{
						ImageView pointImageView=(ImageView)root.lookup("#p_"+i+"_"+j);
						Pane areaPane=(Pane)root.lookup("#chessArea");
						areaPane.getChildren().remove(pointImageView);
						pointImageView.setOnMouseClicked(moveChessOnClick);
						pointImageView.setVisible(true);
						areaPane.getChildren().add(pointImageView);
						NeedUpLevelChessPoints.add("up_f"+thislevel+"_"+i+"_"+j);
					}
				}


		}

	};

	/**************************** 核心事件5：移动棋子图片到同一层的操作 ********************************************/

	static EventHandler<MouseEvent> moveChessOnClick=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {


			/*隐藏所有落子点*/
			String a[]=new String [1];
			diableAvilablePoints(a);
			/*获取被点击的落子点*/
			ImageView iView=(ImageView) event.getSource();
			String data[] =iView.getId().split("_");
			XYdata[0]=Integer.valueOf(data[1]);
			XYdata[1]=Integer.valueOf(data[2]);
			XYLocation[0]=Integer.valueOf((int) iView.getLayoutX());
			XYLocation[1]=Integer.valueOf((int) iView.getLayoutY());
			XYRelative[0]=Integer.valueOf((int) iView.getX());
			XYRelative[0]=Integer.valueOf((int) iView.getY());

			/*获取该落子点上是否有棋子*/
		//	if(IsPointHavaAChess(XYdata[0],XYdata[1],level)){level++;}
			/*如果这个落子点是向上层落子的话，则取一个方向*/
			int dir=0;
			if(NeedUpLevelChessPoints.contains("up_f"+level+"_"+XYdata[0]+"_"+XYdata[1]))
			{
				/*获得一个方向*/
				dir=getUpLevelDiration(level,NowChessType);

				if(level<3) level++;
				/*获得旋转按钮的事件*/
				 reverseBindRotateEvent();
				 isUpNeedRotate=true;
			}
			else {
				 dir =fitableChessPositionProvider(NowchessDir);
			}
			/*获取要移动的棋子*/
			Node oldChess=root.lookup("#"+nodeNameString);
			oldChess.setId("tempChessImg");
			oldChess.setOpacity(1);


			ImageView pointImageView=(ImageView)root.lookup("#tempChessImg");
			Pane areaPane=(Pane)root.lookup("#chessArea");
			areaPane.getChildren().remove(pointImageView);
			pointImageView.setVisible(true);
			areaPane.getChildren().add(pointImageView);


			pointImageView.setTranslateY(AppendChessImg.getChessLocationY(NowChessType,XYLocation[0],XYLocation[1],NowchessDir)-oldChess.getLayoutY());
			pointImageView.setTranslateX(AppendChessImg.getChessLocationX(NowChessType,XYLocation[0],XYLocation[1],NowchessDir)-oldChess.getLayoutX());
			pointImageView.getTransforms().add(new Rotate(getRotateValue(NowchessDir,dir),AppendChessImg.getSuitableRotateX(NowChessType, iView.getX()),AppendChessImg.getSuitableRotateY(NowChessType, iView.getY())));
			NowchessDir=dir;

			/*想移动棋子已经走了*/
			CancalType=3;ManagedButtons();
			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮
		}

//		private boolean IsPointHavaAChess(int i, int j, int level) {
//			if(chessBoardModel.chessBoard.chessPoints[level][i][j].chessNode.chessName=="") return true;
//			return false;
//		}

	};

	/**************************** 核心事件处理6：用户确定移动棋子后修改棋盘模型,保存数据***********************/
	static EventHandler<MouseEvent> userConfirmMoveEvent=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {


			/*把原来棋子的占用空间初始化为null，表示他们不再占用这些地方*/
			/*数据结构链表那样的算法*/
			/*11-21*/
//			chessBoardModel.chessBoard.chessPoints[oldDataXY[0]][oldDataXY[1]].chessNode.next.next=null;
//			System.gc();
//			chessBoardModel.chessBoard.chessPoints[oldDataXY[0]][oldDataXY[1]].chessNode.next=null;
//			System.gc();
//			chessBoardModel.chessBoard.chessPoints[oldDataXY[0]][oldDataXY[1]].chessNode=null;
//			try {
//				((com.punct.controlers.Node)OldChessList.get(3*OldChessListIndex)).finalize();
//			} catch (Throwable e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			OldChessList.get(3*OldChessListIndex+1);
//			OldChessList.get(3*OldChessListIndex+2);
//			Object out=new Object();
//			/*清理内存，这个方法弄了好久了，折中解决吧*/
//			ClearPoints.chessType=NowChessType;
//			ClearPoints.clear(ClearPoints.ClearTempDir, oldDataXY);

			for(int i=0;i<6;i++)
			{
				if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
				{
					/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
					Pane pane=(Pane)root.lookup("#"+NowChessID);
					pane.setOnMouseExited(PaneBorderOutEffect);

					switch (i) {
					case 0: LineTop lineTop=new LineTop();
							setLineTop(lineTop,NowChessID+"_"+steps+"_"+level,level);

					break;
					case 1: LineCenter lineCenter=new LineCenter();
					setLineCenter(lineCenter,NowChessID+"_"+steps+"_"+level,level);
					break;
					case 2: Triangle triangle=new Triangle();
							setTriangle(triangle,NowChessID+"_"+steps+"_"+level,level);

					break;
					case 3:	ArchTop archTop=new ArchTop();
							setArchTop(archTop,NowChessID+"_"+steps+"_"+level,level);
					break;
					case 4:ArchCenter archCenter=new ArchCenter();
						setArchCenter(archCenter,NowChessID+"_"+steps+"_"+level,level);
					break;
					case 5:ArchBottom archBottom=new ArchBottom();
					setArchBottom(archBottom,NowChessID+"_"+steps+"_"+level,level);

					break;
					default:
						break;
					};
				}
			}

			SaveMoveDataToSQL(NowChessID,NowChessID+"_"+steps+"_"+level,XYdata,NowchessDir,color,level,steps,NowChessType,oldDataXY,OldchessDir,oldChessName,oldLevel);//数据保存到数据库中
			/*通用操作*/
			modelStack.push(chessBoardModel);
			stepChessName.push(NowChessID+"_"+steps+"_"+level);
			ImageView tempV=(ImageView)root.lookup("#tempChessImg");
			tempV.setId(NowChessID+"_"+steps+"_"+level);
			steps++;
			/*反转角色*/
			ativeAnother();
			color=ColorControler.reverseColor(color);
			rotateProvoder=0;



			moveSwitch=0;
			InitChessPointsInit();
			/*失去确定按钮绑定事件*/
			//reverseBindComfirmEvent();
			/*失去上层旋转事件*/
			if(isUpNeedRotate)
				reverseBindRotateEvent();
			setAtiveMoveActionChessPoints();
			/*清空之前的存储的点*/
			/*修改完数据之后，设置level为0*/
			level=0;
			/*清理包含可向上层落子的落子点*/
			NeedUpLevelChessPoints.clear();
			/*初始化取消事件的参数*/
			CancalType=-1;
			ManagedButtons();
			isTouchCancelBtn=false;//表示重新开始但是还没点击过取消下棋的按钮
		}
	};

	/**************************** 鼠标滑过出选中边框事件 **************************/

	static EventHandler<MouseEvent> PaneBorderOutEffect = new EventHandler<MouseEvent>() {
//事件管理器
		@Override
		public void handle(MouseEvent event) {
			Pane pane=(Pane)event.getSource();
			pane.setStyle(
					"-fx-border-style:dotted;-fx-border-radius:10;-fx-border-color:#333;-fx-border-width:4;"); // 设置css样式

		}
	};
	/**************************** 鼠标滑过下棋提示点特效事件 **************************/
	static EventHandler<MouseEvent> Ineffect = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {
			selectPointsBGM();
			Image image = new Image(getClass().getResource("../view/rond_selection3.png").toExternalForm());
			ImageInput replaceImageInput = new ImageInput();
			replaceImageInput.setSource(image);
			ImageView iView=(ImageView) event.getSource();
			iView.setEffect(replaceImageInput);


		}
	};

	/**************************** 鼠标滑过下棋提示点特效事件 **************************/
	static EventHandler<MouseEvent> Outeffect = new EventHandler<MouseEvent>() {


		// 事件管理器
		@Override
		public void handle(MouseEvent event) {

			Image image = new Image(getClass().getResource("../view/rond_selection2.png").toExternalForm());
			ImageInput replaceImageInput = new ImageInput();
			replaceImageInput.setSource(image);
			ImageView iView=(ImageView) event.getSource();
			iView.setEffect(replaceImageInput);
		}
	};

	/**************************** 取消重新选择棋子下的事件 **************************/

	static EventHandler<MouseEvent> cancelEvent = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event){
			isTouchCancelBtn=true;
			switch(CancalType)
			{
			case 0:
				LoseBindCMoveEvent();
				ativeItselt();diableAvilablePoints(AtiveChessPoints);setAtiveMoveActionChessPoints();
				InitChessPointsInit();
				setAtiveMoveActionChessPoints();
				break;
			case 1:
				LoseBindCMoveEvent();
				ativeItselt();
				ClearPoints.ClearTempDir=NowchessDir;
				/*清理棋子引用的空间,在清空的时候已经有保存，可以撤销*/
				ClearPoints.level=0;
				ClearPoints.chessType=NowChessType;
				ClearPoints.clear(ClearPoints.ClearTempDir, XYdata);
				((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#tempChessImg"));
				addCountone(NowChessID);
				setAtiveMoveActionChessPoints();
				InitChessPointsInit();
				setAtiveMoveActionChessPoints();
				break;
			case 2:
				getBindCMoveEvent();
				modelStack.pop();
				stepChessName.pop();
				String a[]=new String [1];
				diableAvilablePoints(a);
				root.lookup("#"+nodeNameString).setOpacity(1);
				XYdata[0]=oldDataXY[0];
				XYdata[1]=oldDataXY[1];
				for(int i=0;i<6;i++)
				{
					if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
					{
						/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
						Pane pane=(Pane)root.lookup("#"+NowChessID);
						pane.setOnMouseExited(PaneBorderOutEffect);

						switch (i) {
						case 0: LineTop lineTop=new LineTop();
								setLineTop(lineTop,NowChessID+"_"+steps+"_"+level,0);
						break;
						case 1: LineCenter lineCenter=new LineCenter();
								setLineCenter(lineCenter,NowChessID+"_"+steps+"_"+level,0);
						break;
						case 2: Triangle triangle=new Triangle();
								setTriangle(triangle,NowChessID+"_"+steps+"_"+level,0);

						break;
						case 3:	ArchTop archTop=new ArchTop();
								setArchTop(archTop,NowChessID+"_"+steps+"_"+level,0);
						break;
						case 4:ArchCenter archCenter=new ArchCenter();
							setArchCenter(archCenter,NowChessID+"_"+steps+"_"+level,0);
						break;
						case 5:ArchBottom archBottom=new ArchBottom();
						setArchBottom(archBottom,NowChessID+"_"+steps+"_"+level,0);

						break;
						default:
							break;
						};
					}
				}

				modelStack.push(chessBoardModel);
				stepChessName.push(NowChessID+"_"+steps+"_"+level);
				ImageView tempV=(ImageView)root.lookup("#"+nodeNameString);
				tempV.setId(NowChessID+"_"+steps+"_"+level);
				ativeItselt();
				setAtiveMoveActionChessPoints();
				LoseBindCMoveEvent();

				break;
			case 3:
				getBindCMoveEvent();
				modelStack.pop();
				stepChessName.pop();
				String q[]=new String [1];
				diableAvilablePoints(q);
				((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#tempChessImg"));
				XYdata[0]=oldDataXY[0];
				XYdata[1]=oldDataXY[1];
				NowchessDir=OldchessDir;
				for(int i=0;i<6;i++)
				{
					if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
					{
						/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
						Pane pane=(Pane)root.lookup("#"+NowChessID);
						pane.setOnMouseExited(PaneBorderOutEffect);

						switch (i) {
						case 0: LineTop lineTop=new LineTop();
								setLineTop(lineTop,nodeNameString,0);
						break;
						case 1: LineCenter lineCenter=new LineCenter();
								setLineCenter(lineCenter,nodeNameString,0);
						break;
						case 2: Triangle triangle=new Triangle();
								setTriangle(triangle,nodeNameString,0);

						break;
						case 3:	ArchTop archTop=new ArchTop();
								setArchTop(archTop,nodeNameString,0);
						break;
						case 4:ArchCenter archCenter=new ArchCenter();
							setArchCenter(archCenter,nodeNameString,0);
						break;
						case 5:ArchBottom archBottom=new ArchBottom();
						setArchBottom(archBottom,nodeNameString,0);

						break;
						default:
							break;
						};
					}
				}

				modelStack.push(chessBoardModel);
				stepChessName.push(NowChessID+"_"+steps+"_"+level);

				ImageView iView=(ImageView)root.lookup("#p_"+XYdata[0]+"_"+XYdata[1]);
				int x=AppendChessImg.getChessLocationX(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),OldchessDir);
				int y=AppendChessImg.getChessLocationY(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),OldchessDir);
				ImageView newChessImg=new ImageView();
				String chessImgURL=AppendChessImg.getChessImgURL(NowChessID);
				newChessImg.setImage(new Image(getClass().getResource("../view/"+chessImgURL).toExternalForm()));
				newChessImg.setLayoutX(x);
				newChessImg.setLayoutY(y);
				newChessImg.setVisible(true);
				newChessImg.setId("tempChessImg");
				//根据判断函数传送回来的合适的方向进行旋转构造方法： Rotate(double angle, double pivotX, double pivotY) 参数是角度，以那个点为旋转轴心
				newChessImg.getTransforms().add(new Rotate(getRotateValue(0,OldchessDir),AppendChessImg.getSuitableRotateX(NowChessType, iView.getX()),AppendChessImg.getSuitableRotateY(NowChessType, iView.getY())));
				Pane temp=(Pane)root.lookup("#chessArea");
				temp.getChildren().add(newChessImg);

				newChessImg.setId(nodeNameString);
				ativeItselt();
				setAtiveMoveActionChessPoints();
				/*失去确定按钮绑定事件*/
				//reverseBindComfirmEvent();
				/*失去上层旋转事件*/
				if(isUpNeedRotate)
					reverseBindRotateEvent();
				isUpNeedRotate=false;

				break;

				default: break;
			}
			CancalType=5;
			ManagedButtons();

		}


	};


	/**************************** 取消重新选择棋子下的事件 **************************/

	/***************************************悔棋*************************************/
	static EventHandler<MouseEvent> RegretEvent = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event){
			if(!isTouchCancelBtn){
				switch(CancalType)
				{
				case 0:
					LoseBindCMoveEvent();
					ativeItselt();diableAvilablePoints(AtiveChessPoints);
					InitChessPointsInit();
					setAtiveMoveActionChessPoints();
					break;
				case 1:
					LoseBindCMoveEvent();
					ativeItselt();
					ClearPoints.ClearTempDir=NowchessDir;
					/*清理棋子引用的空间,在清空的时候已经有保存，可以撤销*/
					ClearPoints.level=0;
					ClearPoints.chessType=NowChessType;
					ClearPoints.clear(ClearPoints.ClearTempDir, XYdata);
					((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#tempChessImg"));
					setAtiveMoveActionChessPoints();
					InitChessPointsInit();
					setAtiveMoveActionChessPoints();
					//addCountone(NowChessID);
					break;
				case 2:
					getBindCMoveEvent();
					modelStack.pop();
					stepChessName.pop();
					String a[]=new String [1];
					diableAvilablePoints(a);
					root.lookup("#"+nodeNameString).setOpacity(1);
					XYdata[0]=oldDataXY[0];
					XYdata[1]=oldDataXY[1];
					for(int i=0;i<6;i++)
					{
						if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
						{
							/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
							Pane pane=(Pane)root.lookup("#"+NowChessID);
							pane.setOnMouseExited(PaneBorderOutEffect);

							switch (i) {
							case 0: LineTop lineTop=new LineTop();
									setLineTop(lineTop,NowChessID+"_"+steps+"_"+level,0);
							break;
							case 1: LineCenter lineCenter=new LineCenter();
									setLineCenter(lineCenter,NowChessID+"_"+steps+"_"+level,0);
							break;
							case 2: Triangle triangle=new Triangle();
									setTriangle(triangle,NowChessID+"_"+steps+"_"+level,0);

							break;
							case 3:	ArchTop archTop=new ArchTop();
									setArchTop(archTop,NowChessID+"_"+steps+"_"+level,0);
							break;
							case 4:ArchCenter archCenter=new ArchCenter();
								setArchCenter(archCenter,NowChessID+"_"+steps+"_"+level,0);
							break;
							case 5:ArchBottom archBottom=new ArchBottom();
							setArchBottom(archBottom,NowChessID+"_"+steps+"_"+level,0);

							break;
							default:
								break;
							};
						}
					}

					modelStack.push(chessBoardModel);
					stepChessName.push(NowChessID+"_"+steps+"_"+level);
					ImageView tempV=(ImageView)root.lookup("#"+nodeNameString);
					tempV.setId(NowChessID+"_"+steps+"_"+level);
					ativeItselt();
					setAtiveMoveActionChessPoints();
					LoseBindCMoveEvent();

					break;
				case 3:
					getBindCMoveEvent();
					modelStack.pop();
					stepChessName.pop();
					String q[]=new String [1];
					diableAvilablePoints(q);
					((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#tempChessImg"));
					XYdata[0]=oldDataXY[0];
					XYdata[1]=oldDataXY[1];
					NowchessDir=OldchessDir;
					for(int i=0;i<6;i++)
					{
						if(NowChessID.equals("pane_white_"+i) || NowChessID.equals("pane_black_"+i))
						{
							/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
							Pane pane=(Pane)root.lookup("#"+NowChessID);
							pane.setOnMouseExited(PaneBorderOutEffect);

							switch (i) {
							case 0: LineTop lineTop=new LineTop();
									setLineTop(lineTop,nodeNameString,0);
							break;
							case 1: LineCenter lineCenter=new LineCenter();
									setLineCenter(lineCenter,nodeNameString,0);
							break;
							case 2: Triangle triangle=new Triangle();
									setTriangle(triangle,nodeNameString,0);

							break;
							case 3:	ArchTop archTop=new ArchTop();
									setArchTop(archTop,nodeNameString,0);
							break;
							case 4:ArchCenter archCenter=new ArchCenter();
								setArchCenter(archCenter,nodeNameString,0);
							break;
							case 5:ArchBottom archBottom=new ArchBottom();
							setArchBottom(archBottom,nodeNameString,0);

							break;
							default:
								break;
							};
						}
					}

					modelStack.push(chessBoardModel);
					stepChessName.push(NowChessID+"_"+steps+"_"+level);

					ImageView iView=(ImageView)root.lookup("#p_"+XYdata[0]+"_"+XYdata[1]);
					int x=AppendChessImg.getChessLocationX(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),OldchessDir);
					int y=AppendChessImg.getChessLocationY(NowChessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),OldchessDir);
					ImageView newChessImg=new ImageView();
					String chessImgURL=AppendChessImg.getChessImgURL(NowChessID);
					newChessImg.setImage(new Image(getClass().getResource("../view/"+chessImgURL).toExternalForm()));
					newChessImg.setLayoutX(x);
					newChessImg.setLayoutY(y);
					newChessImg.setVisible(true);
					newChessImg.setId("tempChessImg");
					//根据判断函数传送回来的合适的方向进行旋转构造方法： Rotate(double angle, double pivotX, double pivotY) 参数是角度，以那个点为旋转轴心
					newChessImg.getTransforms().add(new Rotate(getRotateValue(0,OldchessDir),AppendChessImg.getSuitableRotateX(NowChessType, iView.getX()),AppendChessImg.getSuitableRotateY(NowChessType, iView.getY())));
					Pane temp=(Pane)root.lookup("#chessArea");
					temp.getChildren().add(newChessImg);

					newChessImg.setId(nodeNameString);
					ativeItselt();
					setAtiveMoveActionChessPoints();
					/*失去确定按钮绑定事件*/
					//reverseBindComfirmEvent();
					/*失去上层旋转事件*/
					if(isUpNeedRotate)
						reverseBindRotateEvent();
					isUpNeedRotate=false;

					break;

					default: break;
				}
			}
			for(int i=0;i<2;i++)
			{
				if(steps>=1)
				{
					String datastr=sql.getBoardState(boardID, steps-1);
					String eachdata[]=datastr.split(";");
					if(eachdata[0].equals("addnew"))
					{
						int xy[]=new int[2];
						String imgname=eachdata[2];
						int x=Integer.parseInt(eachdata[3]);
						int y=Integer.parseInt(eachdata[4]);
						xy[0]=x;xy[1]=y;
						int dir=Integer.parseInt(eachdata[5]);
						int level=Integer.parseInt(eachdata[7]);
						int chessType=Integer.parseInt(eachdata[9]);
						((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#"+imgname));
						ClearPoints.ClearTempDir=dir;
						/*清理棋子引用的空间,在清空的时候已经有保存，可以撤销*/
						ClearPoints.level=level;
						ClearPoints.chessType=chessType;
						ClearPoints.clear(ClearPoints.ClearTempDir, xy);
						addCountone(eachdata[1]);
						steps--;
					}
					else
					{
						int xy[]=new int[2];
						String imgname=eachdata[2];
						String NowchessID2=eachdata[1];
						int a=Integer.parseInt(eachdata[3]);
						int b=Integer.parseInt(eachdata[4]);
						xy[0]=a;xy[1]=b;
						int dir=Integer.parseInt(eachdata[5]);
						int level=Integer.parseInt(eachdata[7]);

						int chessType=Integer.parseInt(eachdata[9]);
						((Pane)root.lookup("#chessArea")).getChildren().remove(root.lookup("#"+imgname));

						String oldChessName=eachdata[13];
						int oldX=Integer.parseInt(eachdata[10]);
						int oldY=Integer.parseInt(eachdata[11]);
						int oldDir=Integer.parseInt(eachdata[12]);
						int oldLevel=Integer.parseInt(eachdata[14]);

						ClearPoints.ClearTempDir=dir;
						/*清理棋子引用的空间,在清空的时候已经有保存，可以撤销*/
						ClearPoints.level=level;
						ClearPoints.chessType=chessType;
						ClearPoints.clear(ClearPoints.ClearTempDir, xy);
						steps--;

						/*恢复原来图片的位置*/
						ImageView iView=(ImageView)root.lookup("#p_"+oldX+"_"+oldY);
						/*开始增加图片，设置新棋子在棋盘中的坐标*/
						int x=AppendChessImg.getChessLocationX(chessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),dir);
						int y=AppendChessImg.getChessLocationY(chessType, (int)iView.getLayoutX(),(int)iView.getLayoutY(),dir);
						ImageView newChessImg=new ImageView();
						String chessImgURL=AppendChessImg.getChessImgURL(NowchessID2);
						newChessImg.setImage(new Image(getClass().getResource("../view/"+chessImgURL).toExternalForm()));
						newChessImg.setLayoutX(x);
						newChessImg.setLayoutY(y);
						newChessImg.setVisible(true);
						newChessImg.setId(oldChessName);
						//根据判断函数传送回来的合适的方向进行旋转构造方法： Rotate(double angle, double pivotX, double pivotY) 参数是角度，以那个点为旋转轴心
						newChessImg.getTransforms().add(new Rotate(getRotateValue(0,oldDir),AppendChessImg.getSuitableRotateX(chessType, iView.getX()),AppendChessImg.getSuitableRotateY(chessType, iView.getY())));
						Pane temp=(Pane)root.lookup("#chessArea");
						temp.getChildren().add(newChessImg);

						/*在原来的点再下一次*/
						XYdata[0]=oldX;
						XYdata[1]=oldY;
						NowchessDir=oldDir;
						NowChessID=NowchessID2;
						StandaloneBegin.level=oldLevel;
						//StandaloneBegin.level=oldLevel;
						for(int i1=0;i1<6;i1++)
						{
							if(NowChessID.equals("pane_white_"+i1) || NowChessID.equals("pane_black_"+i1))
							{
								/*反转颜色，需要重新把刚刚那个固定红色边框的棋子的CSS修改，并初始化对方的棋子*/
								Pane pane=(Pane)root.lookup("#"+NowChessID);
								pane.setOnMouseExited(PaneBorderOutEffect);

								switch (i1) {
								case 0: LineTop lineTop=new LineTop();
										setLineTop(lineTop,oldChessName,oldLevel);
								break;
								case 1: LineCenter lineCenter=new LineCenter();
										setLineCenter(lineCenter,oldChessName,oldLevel);
								break;
								case 2: Triangle triangle=new Triangle();
										setTriangle(triangle,oldChessName,oldLevel);

								break;
								case 3:	ArchTop archTop=new ArchTop();
										setArchTop(archTop,oldChessName,oldLevel);
								break;
								case 4:ArchCenter archCenter=new ArchCenter();
									setArchCenter(archCenter,oldChessName,oldLevel);
								break;
								case 5:ArchBottom archBottom=new ArchBottom();
								setArchBottom(archBottom,oldChessName,oldLevel);

								break;
								default:
									break;
								};
							}
						}
					}

				}
				else
				{
					System.out.println("一开始了还想悔棋？？？？exm？？？");
				}
				sql.deleteStates(boardID,steps);

			}
			String q[]=new String [1];
			diableAvilablePoints(q);
			InitChessPointsInit();
			setAtiveMoveActionChessPoints();
		}



	};




	protected static void SaveMoveDataToSQL(String nowChessID2, String imgName, int[] xYdata2, int nowchessDir2,
			int color2, int level2, int steps2, int chessType,int[] oldDataXY2, int oldchessDir2,String oldChessName,int oldLevel) {

		String state="moveold"+";"+nowChessID2+";"+imgName+";"+xYdata2[0]+";"+xYdata2[1]+";"+nowchessDir2+";"+color2+";"+level2+";"+steps2+";"+chessType+";"+oldDataXY2[0]+";"+oldDataXY2[1]+";"+oldchessDir2+";"+oldChessName+";"+oldLevel;
		sql.addBoardState(state, steps, boardID);
	}


	protected static void SaveAddDataTOSQL(String nowChessID2, String imgName, int[] xYdata2, int nowchessDir2, int color2, int level2,int steps,int chessType) {

		String state="addnew"+";"+nowChessID2+";"+imgName+";"+xYdata2[0]+";"+xYdata2[1]+";"+nowchessDir2+";"+color2+";"+level2+";"+steps+";"+chessType;
		sql.addBoardState(state, steps, boardID);


	}


	protected static int getUpLevelDiration(int thislevel,int nowChessType) {
		String result=ChessMoveToUpLevelRotateDirProvider.getDir(thislevel,XYdata[0],XYdata[1],NowChessType);
		char p[]=result.toCharArray();
		int length=p.length;
		int re=0;
		re=rotateProvoder%length;
		rotateProvoder++;
		if(Math.abs(Integer.valueOf(p[re]-48))==NowchessDir && length!=1)
			return Math.abs(Integer.valueOf(p[rotateProvoder%length]-48));
		else {
			return Math.abs(Integer.valueOf(p[re]-48));
		}


	}

	/*反转确定按钮的的绑定事件*/
	private static void reverseBindComfirmEvent() {
		if(flag)
		{

			root.lookup("#Comfirm").setOnMouseClicked(userConfirmMoveEvent);
			flag=!flag;
		}
		else {
			root.lookup("#Comfirm").setOnMouseClicked(userConClickEvent);
			flag=!flag;
		}

	}
	/*反转确定按钮的的绑定事件*/
	private static void LoseBindCMoveEvent() {

			root.lookup("#Comfirm").setOnMouseClicked(userConClickEvent);

	}
	/*反转确定按钮的的绑定事件*/
	private static void getBindCMoveEvent() {

			root.lookup("#Comfirm").setOnMouseClicked(userConfirmMoveEvent);

	}
	/*反转确定按钮的的绑定事件*/
	private static void reverseBindRotateEvent() {
		if(flag2)
		{

			root.lookup("#RotateChess").setOnMouseClicked(rotateConClickEvent2);
			flag2=!flag2;
		}
		else {
			root.lookup("#RotateChess").setOnMouseClicked(rotateConClickEvent);
			flag2=!flag2;
		}

	}
	/*获取棋子旋转的角度*/
	protected static double getRotateValue(int olddir,int newdir) {
		switch(Math.abs(olddir)){
		case 0:
			switch (newdir) {
			case 0:return 0;
			case 1:return 60;
			case 2:return 120;
			case 3:return 180;
			case 4:return 240;
			case 5:return 300;
			default:
				break;
			}
		case 1:
			switch (newdir) {
			case 0:return 300;
			case 1:return 0;
			case 2:return 60;
			case 3:return 120;
			case 4:return 180;
			case 5:return 240;
			default:
				break;
			}
		case 2:
			switch (newdir) {
			case 0:return 240;
			case 1:return 300;
			case 2:return 0;
			case 3:return 60;
			case 4:return 120;
			case 5:return 180;
			default:
				break;
			}
		case 3:
			switch (newdir) {
			case 0:return 180;
			case 1:return 240;
			case 2:return 300;
			case 3:return 0;
			case 4:return 60;
			case 5:return 120;
			default:
				break;
			}
		case 4:
			switch (newdir) {
			case 0:return 120;
			case 1:return 180;
			case 2:return 240;
			case 3:return 300;
			case 4:return 0;
			case 5:return 60;
			default:
				break;
			}
		case 5:
			switch (newdir) {
			case 0:return 60;
			case 1:return 120;
			case 2:return 180;
			case 3:return 240;
			case 4:return 300;
			case 5:return 0;
			default:
				break;
			}

		default:break;
		}
		return 0;
	}
	/*设置棋子面板初始化的状态 */
	private static void InitChessClickEvent() {
		if(color==1)
			InitWhitePane();
		if (color==-1) {
			InitBlackPane();
		}

	}
	private static void InitBlackPane() {

		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_black_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			node.setOnMouseEntered(PaneBorderInEffect);
			node.setOnMouseExited(PaneBorderOutEffect);
			node.setOnMouseClicked(clickChess);
		}

	}
	private static void InitWhitePane() {
		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_white_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			node.setOnMouseEntered(PaneBorderInEffect);
			node.setOnMouseExited(PaneBorderOutEffect);
			node.setOnMouseClicked(clickChess);

		}

	}

	/*堵塞对方的棋子几个函数，我方只能看到对方棋子数量的变化，并不能操控对方的棋子*/
	private static void disableBlack() {
		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_black_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			node.setDisable(true);
			node.setStyle("");
		}

	}
	private static void disableWhite() {
		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_white_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			node.setDisable(true);
			node.setStyle("");
		}

	}
	private void disableAnother() {
		if(color==1)
			disableBlack();
		if (color==-1) {
			disableWhite();
		}

	}
	private static void disableAllColor() {//全部
			disableBlack();
			disableWhite();

	}
	/*激活棋子的可操作性。分别有激活白色的，激活黑色的，激活对方。激活要在棋子数量大于0的时候才能激活*/
	private static void ativeBlack()
	{
		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_black_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			Label counterI=(Label)root.lookup("#"+"lab_black_counter_"+i);

			if(Integer.valueOf(counterI.getText())>0)
			{

				node.setStyle("-fx-border-style:dotted;-fx-border-radius:10;-fx-border-color:#333;-fx-border-width:4;"); // 设置css样式");
				node.setDisable(false);
				node.setOnMouseEntered(PaneBorderInEffect);
				node.setOnMouseExited(PaneBorderOutEffect);
				node.setOnMouseClicked(clickChess);

			}
		}

	}
	private static void ativeWhite()
	{
		for(int i=0;i<6;i++)
		{
			String paneNameString="pane_white_"+i;
			Node node=root.lookup("#"+paneNameString+"");
			Label counterI=(Label)root.lookup("#"+"lab_white_counter_"+i);

			if(Integer.valueOf(counterI.getText())>0)
			{
				node.setDisable(false);
				node.setStyle("-fx-border-style:dotted;-fx-border-radius:10;-fx-border-color:#333;-fx-border-width:4;"); // 设置css样式);
				node.setOnMouseEntered(PaneBorderInEffect);
				node.setOnMouseExited(PaneBorderOutEffect);
				node.setOnMouseClicked(clickChess);
			}
		}

	}
	private static void ativeAnother() {
		if(color==1)
			ativeBlack();
		if (color==-1) {
			ativeWhite();
		}

	}
	private static void ativeItselt() {
		if(color==1)
			ativeWhite();
		if (color==-1) {
			ativeBlack();
		}

	}
	private static void ativeAllColor() {//全部
		ativeBlack();
		ativeWhite();

	}

	/*初始化所有可落子点，落子点增加点击事件*/
	private static void InitChessPointsInit() {
		for(int k=0;k<3;k++)
		for (int i = 0; i < 17; i++) {
			for (int j = 0; j < StandaloneBegin.chessBoardModel.chessBoard.chessPoints[k][i].length; j++) {
				String tempString ="p_"+i+"_"+j;
				root.lookup("#"+tempString).setOnMouseClicked(clickPoints);
				root.lookup("#"+tempString).setOnMouseEntered(Ineffect);
				root.lookup("#"+tempString).setOnMouseExited(Outeffect);
			}
		}

	}
	/*显示能下棋的棋子的空间*/
	private static void EnableAvailablePoints(String[] availablePoints) {

		for(int i=0;i<availablePoints.length;i++)
		{
			Node tempNode=root.lookup("#"+availablePoints[i]);
			tempNode.setVisible(true);
		}

	}
	/*下棋完毕后隐藏可下棋的空间*/
	protected static void diableAvilablePoints(String[] ativeChessPoints2) {
		for(int k=0;k<3;k++)
		for (int i = 0; i < 17; i++) {
			for (int j = 0; j < StandaloneBegin.chessBoardModel.chessBoard.chessPoints[k][i].length; j++) {
				{
					Node tempNode=root.lookup("#"+"p_" + i + "_" + j);
					tempNode.setVisible(false);
				}
			}
		}
//		for(int i=0;i<ativeChessPoints2.length;i++)
//		{
//			Node tempNode=root.lookup("#"+ativeChessPoints2[i]);
//			tempNode.setVisible(false);
//		}

	}
	/*获取的棋子合适的位置*/
	private static int fitableChessPositionProvider(int origal)
	{
		String result=ChessRotatePositionProvider.getDiraction(NowChessType,XYdata[0],XYdata[1],level);
		char p[]=result.toCharArray();
		int length=p.length;
		int re=0;
		re=rotateProvoder%length;
		rotateProvoder++;
		if(Math.abs(Integer.valueOf(p[re]-48))==origal && length!=1)
			fitableChessPositionProvider(origal);
		else
			return Math.abs(Integer.valueOf(p[re]-48));
		return Math.abs(Integer.valueOf(p[re]-48));
	}
	/*(高亮所有可以选择的棋子)玩家的第二个操作：移动棋子高亮的选择可落子点部分*/
	private static void setAtiveMoveActionChessPoints()
	{
		/*0代表玩家想移动棋子，1代表玩家想下新的棋子*/
		if(moveSwitch==0)
		{
			for(int k=0;k<2;k++)
			for (int i = 0; i < 17; i++) {
				for (int j = 0; j < StandaloneBegin.chessBoardModel.chessBoard.chessPoints[k][i].length; j++) {

					if(chessBoardModel.chessBoard.chessPoints[k][i][j].chessNode!=null && StandaloneBegin.chessBoardModel.chessBoard.chessPoints[k][i][j].chessNode.getDatax()!=-1 && StandaloneBegin.chessBoardModel.chessBoard.chessPoints[k][i][j].chessNode.color==color)
					{
						//if(chessBoardModel.chessBoard.chessPoints[0][i][j].chessNode!=null && chessBoardModel.chessBoard.chessPoints[1][i][j].chessNode!=null) {;}
						//else{

							if(!JudgeIsCovered.IsCovered( i, j,k, chessBoardModel.chessBoard.chessPoints[k][i][j].chessNode.dir, chessBoardModel.chessBoard.chessPoints[k][i][j].chessNode.type))
							{
								String pointNameString="p_"+ChessBoard.chessPoints[k][i][j].chessNode.getDatax()+"_"+ChessBoard.chessPoints[k][i][j].chessNode.getDatay();
								Node node=root.lookup("#"+pointNameString);
								ImageView iView=(ImageView)node;
								iView.setVisible(true);
								Pane pane=(Pane)root.lookup("#chessArea");
								pane.getChildren().remove(iView);
								iView.setOnMouseClicked(moveChessPreClick);
								pane.getChildren().add(iView);
								OldChessList.add(chessBoardModel.chessBoard.chessPoints[level][i][j]);
								OldChessListIndex++;
							}
							else {
								continue;
							}

						//}
					}
				}
			}

		}
		if(moveSwitch==1)
		{
			;
		}


	}


	private static void ManagedButtons(){
		switch (CancalType) {
		case 0:
			root.lookup("#RestartMyTurn").setDisable(false);
			root.lookup("#RotateChess").setDisable(true);
			root.lookup("#Comfirm").setDisable(true);
			root.lookup("#GiveUp").setDisable(false);
			root.lookup("#RegretChess").setDisable(false);
			break;
		case 1:
			root.lookup("#RestartMyTurn").setDisable(false);
			root.lookup("#RotateChess").setDisable(false);
			root.lookup("#Comfirm").setDisable(false);
			root.lookup("#GiveUp").setDisable(false);
			root.lookup("#RegretChess").setDisable(false);
			break;
		case 2:
			root.lookup("#RestartMyTurn").setDisable(false);
			root.lookup("#RotateChess").setDisable(true);
			root.lookup("#Comfirm").setDisable(true);
			root.lookup("#GiveUp").setDisable(false);
			root.lookup("#RegretChess").setDisable(false);
			break;
		case 3:
			root.lookup("#RestartMyTurn").setDisable(false);
			root.lookup("#RotateChess").setDisable(false);
			root.lookup("#Comfirm").setDisable(false);
			root.lookup("#GiveUp").setDisable(false);
			root.lookup("#RegretChess").setDisable(false);
			break;
		case -1:
			root.lookup("#RestartMyTurn").setDisable(true);
			root.lookup("#RotateChess").setDisable(true);
			root.lookup("#Comfirm").setDisable(true);
			root.lookup("#GiveUp").setDisable(true);
			root.lookup("#RegretChess").setDisable(true);
			break;
		case 5:
			root.lookup("#RestartMyTurn").setDisable(true);
			root.lookup("#RotateChess").setDisable(true);
			root.lookup("#Comfirm").setDisable(true);
			root.lookup("#GiveUp").setDisable(false);
			root.lookup("#RegretChess").setDisable(false);

		default:
			break;
		}
	}

	/*为移动棋子的操作的下棋点增加新的事件*/

	private static void addCountone(String chessID)
	{
		String chessNum[]=chessID.split("_");
		Label counter_laber=(Label)root.lookup("#lab_"+chessNum[1]+"_counter_"+chessNum[2]);
		String aString=String.valueOf(Integer.valueOf(counter_laber.getText())+1);
		counter_laber.setText(aString);
	}
	private static void setMoveActionChessEvent() {


	}

	protected static void setLineCenter(LineCenter lineCenter, String ChessName,int thisLevel) {

		lineCenter.setColor(color);
		lineCenter.setDir(NowchessDir);
		lineCenter.setLevel(level);
		lineCenter.setOrder(steps);
		lineCenter.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		lineCenter.center.setDatax(XYdata[0]);
		lineCenter.center.setDatay(XYdata[1]);
		lineCenter.center.dir=NowchessDir;
		lineCenter.center.chessName=ChessName;
		lineCenter.center.color=color;
		lineCenter.center.index=steps;
		lineCenter.center.level=level;
		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		lineCenter.top.color=color;
		lineCenter.buttom.color=color;
		switch (NowchessDir)
		{
		case 0:chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = lineCenter.buttom ;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = lineCenter.top;
		break;
		case 1:
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;
		if(XYdata[0]<8)
		{
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = lineCenter.buttom ;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = lineCenter.top;
		}
		else if(XYdata[0]==8)
		{
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = lineCenter.buttom;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;

		}
		else
		{
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;

		}
		break;
		case 2:
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineCenter.center;
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = lineCenter.top;
			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = lineCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = lineCenter.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = lineCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = lineCenter.top;

			}
			break;
		default:break;
		}

	}
	/*设置棋子的初始化条件。等待加入棋盘模型中*/
	protected static void setLineTop(LineTop lineTop,String ChessName,int thisLevel) {

		lineTop.setColor(color);
		lineTop.setDir(NowchessDir);
		lineTop.setLevel(level);
		lineTop.setOrder(steps);
		lineTop.top.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		lineTop.top.setDatax(XYdata[0]);
		lineTop.top.setDatay(XYdata[1]);
		lineTop.top.dir=NowchessDir;
		lineTop.top.chessName=ChessName;
		lineTop.top.color=color;
		lineTop.top.index=steps;
		lineTop.top.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		lineTop.buttom.color=color;
		lineTop.center.color=color;

		switch (NowchessDir) {
		case 0://0是竖直向下的，然后剩下的12345是顺时针方向的几个剩下的方向
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=	lineTop.top;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode=lineTop.center;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-2].chessNode=lineTop.buttom;
//			OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);
//			OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode);
//			OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-2].chessNode);
			break;
		case 1:
			if(XYdata[0]<9)	{chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-2].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-2].chessNode);}
			else if(XYdata[0]==9) {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode);}

			else {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode);}

			break;
		case 2:
			if(XYdata[0]<9)	{chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode);}

			else if(XYdata[0]==9) {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode);}

			else {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+2].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+2].chessNode);}

			break;
		case 3:
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode=lineTop.center;
			chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+2].chessNode=lineTop.buttom;
			//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);
			//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode);
			//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+2].chessNode);
			break;
		case 4:
			if(XYdata[0]<7)	{chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+2].chessNode=lineTop.buttom;}
			else if(XYdata[0]==7) {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode);}

			else {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode);}
			break;
		case 5:
			if(XYdata[0]<7)	{chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode);}

			else if(XYdata[0]==7) {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode);}

			else {chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=lineTop.top;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode=lineTop.center;chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-2].chessNode=lineTop.buttom;
			}//OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode);OldChessList.add(chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-2].chessNode);}
			break;
		default:
			break;

		}



	}

	protected static void setArchBottom(ArchBottom archBottom, String ChessName,int thisLevel) {
		archBottom.setColor(color);
		archBottom.setDir(NowchessDir);
		archBottom.setLevel(level);
		archBottom.setOrder(steps);
		archBottom.buttom.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archBottom.buttom.setDatax(XYdata[0]);
		archBottom.buttom.setDatay(XYdata[1]);
		archBottom.buttom.dir=NowchessDir;
		archBottom.buttom.chessName=ChessName;
		archBottom.buttom.color=color;
		archBottom.buttom.index=steps;
		archBottom.buttom.level=level;
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archBottom.buttom;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archBottom.top.color=color;
		archBottom.center.color=color;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+2].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =archBottom.top;

			}
			break;
		case 1:
			if(XYdata[0]<7)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==7)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]].chessNode = archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode = archBottom.top;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode = archBottom.top;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode = archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archBottom.top;
			}
			break;
		case 4:
			if(XYdata[0]<=8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode =archBottom.top;

			}
			else if(XYdata[0]==9)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode =archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode =archBottom.top;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archBottom.center;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+2].chessNode = archBottom.top;

			}
			break;
		default:
			break;
		}

	}

	protected static void setArchCenter(ArchCenter archCenter, String ChessName,int thisLevel) {
		archCenter.setColor(color);
		archCenter.setDir(NowchessDir);
		archCenter.setLevel(level);
		archCenter.setOrder(steps);
		archCenter.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archCenter.center.setDatax(XYdata[0]);
		archCenter.center.setDatay(XYdata[1]);
		archCenter.center.dir=NowchessDir;
		archCenter.center.chessName=ChessName;
		archCenter.center.color=color;
		archCenter.center.index=steps;
		archCenter.center.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archCenter.buttom.color=color;
		archCenter.top.color=color;

		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archCenter.center;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}

			break;
		case 1:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.top;
			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archCenter.buttom;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archCenter.top;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.buttom;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;

			}
			break;
		case 4:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode =  archCenter.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode =  archCenter.buttom;
			}
			break;

		case 5:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archCenter.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archCenter.top;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archCenter.buttom;

			}
			break;

		default:
			break;
		}

	}

	protected static void setArchTop(ArchTop archTop, String ChessName,int thisLevel) {
		archTop.setColor(color);
		archTop.setDir(NowchessDir);
		archTop.setLevel(level);
		archTop.setOrder(steps);
		archTop.top.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		archTop.top.setDatax(XYdata[0]);
		archTop.top.setDatay(XYdata[1]);
		archTop.top.dir=NowchessDir;
		archTop.top.chessName=ChessName;
		archTop.top.color=color;
		archTop.top.index=steps;
		archTop.top.level=level;

		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		archTop.buttom.color=color;
		archTop.center.color=color;


		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=archTop.top;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			break;
		case 1:
			if(XYdata[0]<9)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==9)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archTop.center ;
					chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-2][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+2].chessNode =  archTop.buttom;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+2].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			break;
		case 4:
			if(XYdata[0]<7)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]+1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==7)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+2][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode =  archTop.buttom;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode =  archTop.buttom;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = archTop.center ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-2].chessNode =  archTop.buttom;

			}

			break;
		default:
			break;
		}


	}

	protected static void setTriangle(Triangle triangle, String ChessName,int thisLevel) {
		triangle.setColor(color);
		triangle.setDir(NowchessDir);
		triangle.setLevel(level);
		triangle.setOrder(steps);
		triangle.center.type=NowChessType;

		/*该点设置一个chessDir，用于可以恢复棋盘用。*/
		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].setDir(NowchessDir);
		/*该点的头节点的属性在这里设置，下面就会自动指向*/
		triangle.center.setDatax(XYdata[0]);
		triangle.center.setDatay(XYdata[1]);
		triangle.center.dir=NowchessDir;
		triangle.center.chessName=ChessName;
		triangle.center.color=color;
		triangle.center.index=steps;
		triangle.center.level=level;


		/*给另外两个点也设置颜色等属性，方便可以判断是不是我方棋类*/
		triangle.left.color=color;
		triangle.right.color=color;

		chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]].chessNode=triangle.center;
		switch (NowchessDir) {
		case 0:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left ;
						chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.right;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left ;
						chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.right;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = triangle.left ;
						chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			break;
		case 1:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.right;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]+1].chessNode = triangle.right;

			}
			break;
		case 2:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode  = triangle.left;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]+1].chessNode =triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.left;

			}
			break;
		case 3:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]+1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.left;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.left;

			}
			break;
		case 4:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]].chessNode = triangle.right ;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.right ;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.left;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]+1][XYdata[1]-1].chessNode = triangle.right ;

			}
			break;
		case 5:
			if(XYdata[0]<8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.left;

			}
			else if(XYdata[0]==8)
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]-1].chessNode = triangle.left;

			}
			else
			{
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]][XYdata[1]-1].chessNode = triangle.right ;
				chessBoardModel.chessBoard.chessPoints[thisLevel][XYdata[0]-1][XYdata[1]].chessNode = triangle.left;

			}
			break;
		default:
			break;
		}

	}

	/*音效部分*/
	private static void selectPointsBGM() {
		if(musicflag2){
		new Thread() {
			public void run() {
				m.SelectPoints();
			}
		}.start();}
	}

	private static void SelectChessBGM() {
		if(musicflag2){new Thread() {
			public void run() {
				m.SelectChess();
			}
		}.start();}
	}

	private static void PlayChessBGM() {
		if(musicflag2){new Thread() {
			public void run() {
				m.PlayChess();
			}
		}.start();}
	}
	private static void RonateChessBGM() {
		if(musicflag2){new Thread() {
			public void run() {
				m.RonateChess();
			}
		}.start();}
	}
	private void SelectOptionBGM() {
		if(musicflag2){new Thread() {
			public void run() {
				m.SelectOptionBGM();
			}
		}.start();}
	}
	EventHandler<MouseEvent>  MousemoveOn=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg2.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};
	EventHandler<MouseEvent>  MousemoveOFF=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};
	EventHandler<MouseEvent>  MousemoveOFF2=new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent arg0) {
			Image image = new Image(getClass().getResource("../view/musicImg3.png").toExternalForm());
			img_music_0.setImage(image);

		}
	};

	EventHandler<MouseEvent>  Musicclick=new EventHandler<MouseEvent>() {

		@SuppressWarnings("deprecation")
		@Override
		public void handle(MouseEvent arg0) {
			BackgroundMusic.musicThread.resume();

			img_music_0.setOnMouseClicked(MusicOFFclick);
			img_music_0.setOnMouseEntered(MousemoveOFF2);
			img_music_0.setOnMouseExited(MousemoveOFF);
			musicflag=true;
			musicflag2=true;
			Image image2 = new Image(getClass().getResource("../view/musicImg.png").toExternalForm());
			img_music_0.setImage(image2);
			Image image = new Image(getClass().getResource("../view/musicOn.png").toExternalForm());
			img_music_fish.setImage(image);
			rollImg();
		}
	};

	EventHandler<MouseEvent>  MusicOFFclick=new EventHandler<MouseEvent>() {

		@SuppressWarnings("deprecation")
		@Override
		public void handle(MouseEvent arg0) {
			BackgroundMusic.musicThread.suspend();
			img_music_0.setOnMouseClicked(Musicclick);
			img_music_0.setOnMouseEntered(MousemoveOFF);
			img_music_0.setOnMouseExited(MousemoveOn);
			musicflag=false;
			musicflag2=false;
			Image image2 = new Image(getClass().getResource("../view/musicImg3.png").toExternalForm());
			img_music_0.setImage(image2);
			Image image = new Image(getClass().getResource("../view/musicOff.png").toExternalForm());
			img_music_fish.setImage(image);
			rollImg();
		}
	};
	private void rollImg() {

		new Thread() {
			int count=0;
			public void run() {
				while(musicflag)
				{
					count++;
					img_music_fish.setRotate(count%360);
					try {

						sleep(50);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
			}
		}.start();
	}

}
