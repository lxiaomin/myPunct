package com.punct.logic;

import com.punct.SQL.SQL;
import com.punct.bgm.ButtonBGM;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class UserRegister extends Application {
	static Pane root;
	static ButtonBGM m2;
	String message; //定义用于存放提示消息
	static int register_img_couner = 0;
	@FXML
    private TextField textbok_email;

    @FXML
    private Label lab_tip;

    @FXML
    private ImageView img_username1;

    @FXML
    private PasswordField textbox_userpsw;

    @FXML
    private Button loginBtn;

    @FXML
    private ImageView img_registe;

    @FXML
    private ImageView img_userpsw;

    @FXML
    private TextField textbok_userName;

    @FXML
    private ImageView img_username;

    @FXML
    private Button btn_registe;
	boolean uidflag = false, pswflag = false, emailflag = false;

	@FXML
	void event_register(ActionEvent event) {
		Platform.runLater(new Runnable() {
			public void run() {
				try {
					new UserLogin().start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口

	}

	@FXML
	void event_login(ActionEvent event) {
		if (!textbok_userName.getText().equals("") && !textbox_userpsw.getText().equals("")
				&& !textbok_email.getText().equals("")) {
			if (uidflag) {
				if (pswflag) {
					if (emailflag) {
						SQL register = new SQL();
						int i = register.AddUser(textbok_userName.getText(), textbox_userpsw.getText(),
								textbok_email.getText(), "");
						if (i == 1) {
							lab_tip.setText("注册成功");
//							User user = new User(textbok_userName.getText(),
//									textbok_email.getText(), textbok_email.getText(), ImageURL);
						} else
							lab_tip.setText("用户名已存在");
					} else {
						lab_tip.setText("邮箱格式不正确");
					}
				} else {
					lab_tip.setText("密码不能少于6位");
				}
			} else {

			}
		} else {
			lab_tip.setText("资料不能留空哦");
		}

	}

	/**************************** 鼠标滑过登陆按钮特效事件 **************************/
	EventHandler<MouseEvent> Ineffect = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {

			Image image = new Image(getClass().getResource("../view/regiterBtn2.png").toExternalForm());
			ImageInput replaceImageInput = new ImageInput();
			replaceImageInput.setSource(image);
			loginBtn.setEffect(replaceImageInput);
			Choose();
		}
	};

	/**************************** 鼠标滑出登陆按钮特效事件 **************************/
	EventHandler<MouseEvent> Outeffect = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {

			Image image = new Image(getClass().getResource("../view/registerBtn.png").toExternalForm());
			ImageInput replaceImageInput = new ImageInput();
			replaceImageInput.setSource(image);
			loginBtn.setEffect(replaceImageInput);

		}
	};

	EventHandler<MouseEvent> Link = new EventHandler<MouseEvent>() { // 事件管理器
		@Override
		public void handle(MouseEvent event) {

			RegisterBGM();

		}
	};

	 /****************************文本框失去焦点事件**************************/
    ChangeListener<Boolean> loose1 = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean lostFocus, Boolean getfocus) {

			if (lostFocus) {
				    Platform.runLater(new Runnable() {
				     @Override
				     public void run() {

				    	 Verify checkuid=new Verify();
				    	 message="";
				    	 message= checkuid.checkUID(textbok_userName.getText());
				    	 if(message.equals("")) uidflag=true;
				    	 lab_tip.setVisible(true);
				    	 lab_tip.setText(message);
				     }
				    });
				   }

		}
    };

    ChangeListener<Boolean> loose2 = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean lostFocus, Boolean getfocus) {

			if (lostFocus) {
				    Platform.runLater(new Runnable() {
				     @Override
				     public void run() {

				    	 Verify checkpsw=new Verify();
				    	 int judge=-1;	//判断返回来的各种状态
				    	 judge=checkpsw.checkPSW(textbox_userpsw.getText());
				    	 if(judge==0) pswflag=true;
				    	 else if(judge==1) {message="密码长度不得大于16位";pswflag=false;}
				    	 else if(judge==2) {message="密码长度不得少于6位";pswflag=false;}
				    	 else if(judge==3) {message="密码不得包含中文";pswflag=false;}
				    	 else if(judge==4) {message="系统异常";pswflag=false;}
				    	 else  {message="系统异常";pswflag=false;}
				    	 lab_tip.setVisible(true);
				    	 lab_tip.setText(message);
				     }
				    });
				   }

		}
    };
    ChangeListener<Boolean> loose3 = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean lostFocus, Boolean getfocus) {

			if (lostFocus && !textbok_email.getText().equals("")) {
				    Platform.runLater(new Runnable() {
				     @Override
				     public void run() {


				    	 Verify checkemail=new Verify();
				    	 boolean flag= checkemail.checkEmail(textbok_email.getText());
				    	 if(flag==false) message="邮箱格式不正确";
				    	 	else emailflag=flag;
				    	 lab_tip.setVisible(true);
				    	 lab_tip.setText(message);
				     }
				    });
				   }

		}
    };

	@FXML
	private void initialize() {// 这个是初始化方法，就是页面加载的时候就可以执行的方法。

		loginBtn.setOnMouseEntered(Ineffect);
		loginBtn.setOnMouseExited(Outeffect);
		textbok_userName.setPromptText("输入您的账号"); // 设置预显示字符
		textbox_userpsw.setPromptText("输入您的密码");// 设置预显示字符
		textbok_email.setPromptText("输入您的邮箱");// 设置预显示字符
		img_registe.setOnMouseEntered(Link);
		lab_tip.setText("");
		textbok_userName.focusedProperty().addListener(loose1); //添加失去焦点的事件
		textbox_userpsw.focusedProperty().addListener(loose2); //添加失去焦点的事件
		textbok_email.focusedProperty().addListener(loose3); //添加失去焦点的事件
		/* 设置文本框的焦点事件 */
		textbok_userName.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbok_userName.isFocused()) {
					TextBoxBGM();
					Image image = new Image(getClass().getResource("../view/username_input2.png").toExternalForm());
					img_username1.setImage(image);
				} else if (textbok_userName.isFocusTraversable()) {
					Image image = new Image(getClass().getResource("../view/username_input.png").toExternalForm());
					img_username1.setImage(image);
				}

			}
		});

		textbox_userpsw.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbox_userpsw.isFocused()) {
					TextBoxBGM();
					Image image = new Image(getClass().getResource("../view/username_input2.png").toExternalForm());
					img_username.setImage(image);
				} else if (textbox_userpsw.isFocusTraversable()) {
					Image image = new Image(getClass().getResource("../view/username_input.png").toExternalForm());
					img_username.setImage(image);
				}

			}
		});

		textbok_email.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbok_email.isFocused()) {
					TextBoxBGM();
					Image image = new Image(getClass().getResource("../view/username_input2.png").toExternalForm());
					img_userpsw.setImage(image);
				} else if (textbok_email.isFocusTraversable()) {
					Image image = new Image(getClass().getResource("../view/username_input.png").toExternalForm());
					img_userpsw.setImage(image);
				}

			}
		});

		rollImg();
	}

	private void rollImg() {

		new Thread() {
			int count = 0;

			public void run() {
				while (true) {
					count++;
					img_registe.setRotate(count % 360);
					try {

						sleep(50);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	/* 音乐 */
	private void Choose() {
		new Thread() {
			public void run() {
				m2.PlayBubblesMusic("2");
			}
		}.start();
	}

	private void TextBoxBGM() {
		new Thread() {
			public void run() {
				m2.PlayTypeMusic("5");
			}
		}.start();
	}

	private void RegisterBGM() {
		new Thread() {
			public void run() {
				m2.PlayLinkMusic();
			}
		}.start();
	}

	public UserRegister() {

	}

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(UserLogin.class.getResource("../view/userRegister.fxml"));

			root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("用户注册");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.setOnCloseRequest(e -> System.exit(0));// 完全退出
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
			stage.show();
			m2 = new ButtonBGM();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
