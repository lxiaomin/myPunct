package com.punct.logic;



import java.awt.Menu;

import com.punct.SQL.SQL;
import com.punct.bgm.ButtonBGM;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class UserLogin  extends Application {

		public static Player player;
	    static Pane root;
	    static ButtonBGM m2;
	    static int register_img_couner=0;
	 	@FXML
	    private PasswordField textbox_userpsw;

	    @FXML
	    private Button loginBtn;

	    @FXML
	    private ImageView img_registe;

	    @FXML
	    private TextField textbok_userName;

	    @FXML
	    private ImageView img_userpsw;

	    @FXML
	    private ImageView img_username;

	    @FXML
	    private Button btn_registe;
	    @FXML
	    private Label tips;
	    @FXML
	    void event_login(ActionEvent event) {

	    	// 登陆事件
			MD5Util md5Util = new MD5Util();
			String username = textbok_userName.getText();
			String userpsw = md5Util.MD5(textbox_userpsw.getText());
			// 同样加密的方式，验证是否与数据库中的匹配
			if (!username.equals("")) {
				if (!userpsw.equals("")) {
					SQL verifypsw = new SQL();
					int result = verifypsw.LoginVerify(textbok_userName.getText(), userpsw);
					if (result == 2) {

						tips.setVisible(true); // 提示label可见
						tips.setText("登陆成功");

						Platform.runLater(new Runnable() {
							public void run() {
								try {
									new GameMenu().start(new Stage());
									int id=verifypsw.getUserID(textbok_userName.getText());
									player=new Player(textbok_userName.getText(),"",id);
									((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						});
						// new Timeline(new KeyFrame(Duration.millis(2500),ae ->
						// changecase())).play(); //定时器，定时关闭登陆界面

						// 主界面登录
					} else if (result == 1) {
						tips.setVisible(true); // 提示label可见
						tips.setText("*您的密码不正确，请检查重新输入");
					} else {
						tips.setVisible(true); // 提示label可见
						tips.setText("*您的用户名或密码不正确，请检查");
					}
				} else {
					tips.setVisible(true); // 提示label可见
					tips.setText("*密码不能为空");
					textbok_userName.setPromptText("输入您的账号"); // 设置预显示字符

				}
			} else {
				tips.setVisible(true);
				tips.setText("*用户名不能为空");
				textbox_userpsw.setPromptText("输入您的密码");// 设置预显示字符
			}


	    }
	    int i;
	    @FXML
	    void event_register(ActionEvent event) {

	    	Platform.runLater(new Runnable() {
				public void run() {
					try {
						new UserRegister().start(new Stage());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});

			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口


	    }

		/**************************** 文本框事件 **************************/
		EventHandler<KeyEvent> pswbox = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.ENTER) {
					event_login(new ActionEvent());
				} else
					;

			}
		};
		/**************************** 鼠标滑过登陆按钮特效事件 **************************/
		EventHandler<MouseEvent> Ineffect = new EventHandler<MouseEvent>() { // 事件管理器
			@Override
			public void handle(MouseEvent event) {

				Image image=new Image(getClass().getResource("../view/login_button_2.png").toExternalForm());
				ImageInput replaceImageInput=new ImageInput();
				replaceImageInput.setSource(image);
				loginBtn.setEffect(replaceImageInput);
				Choose();

			}
		};

		/**************************** 鼠标滑出登陆按钮特效事件 **************************/
		EventHandler<MouseEvent> Outeffect = new EventHandler<MouseEvent>() { // 事件管理器
			@Override
			public void handle(MouseEvent event) {

				Image image=new Image(getClass().getResource("../view/login_button_1.png").toExternalForm());
				ImageInput replaceImageInput=new ImageInput();
				replaceImageInput.setSource(image);
				loginBtn.setEffect(replaceImageInput);

			}
		};

		EventHandler<MouseEvent> Link = new EventHandler<MouseEvent>() { // 事件管理器
			@Override
			public void handle(MouseEvent event) {

				RegisterBGM();

			}
		};



		@FXML
		private void initialize() {// 这个是初始化方法，就是页面加载的时候就可以执行的方法。

			loginBtn.setOnMouseEntered(Ineffect);
			loginBtn.setOnMouseExited(Outeffect);
			textbok_userName.setPromptText("输入您的账号"); // 设置预显示字符
			textbox_userpsw.setPromptText("输入您的密码");// 设置预显示字符
			btn_registe.setOnMouseEntered(Link);
			textbok_userName.setOnKeyPressed(pswbox);
			textbox_userpsw.setOnKeyPressed(pswbox);
			/*设置文本框的焦点事件*/
			textbok_userName.focusedProperty().addListener(new ChangeListener<Boolean>() {


				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

					if(textbok_userName.isFocused())
					{
						TextBoxBGM();
						Image image=new Image(getClass().getResource("../view/username_input2.png").toExternalForm());
						img_username.setImage(image);
					}
					else if(textbok_userName.isFocusTraversable())
					{
						Image image=new Image(getClass().getResource("../view/username_input.png").toExternalForm());
						img_username.setImage(image);
					}

				}
			});

			textbox_userpsw.focusedProperty().addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

					if(textbox_userpsw.isFocused())
					{
						TextBoxBGM();
						Image image=new Image(getClass().getResource("../view/username_input2.png").toExternalForm());
						img_userpsw.setImage(image);
					}
					else if(textbox_userpsw.isFocusTraversable())
					{
						Image image=new Image(getClass().getResource("../view/username_input.png").toExternalForm());
						img_userpsw.setImage(image);
					}

				}
			});

			rollImg();
		}

		private void rollImg() {

			new Thread() {
				int count=0;
				public void run() {
					while(true)
					{
						count++;
						img_registe.setRotate(count%360);
						try {

							sleep(50);
						} catch (InterruptedException e) {

							e.printStackTrace();
						}
					}
				}
			}.start();
		}

		/*音乐*/
		private void Choose() {
			new Thread() {
				public void run() {
					m2.PlayBubblesMusic("2");
				}
			}.start();
		}

		private void TextBoxBGM() {
			new Thread() {
				public void run() {
					m2.PlayTypeMusic("5");
				}
			}.start();
		}

		private void RegisterBGM() {
			new Thread() {
				public void run() {
					m2. PlayLinkMusic();
				}
			}.start();
		}
		@Override
		public void start(Stage stage) throws Exception {
			try {

				FXMLLoader loader = new FXMLLoader(UserLogin.class
	                    .getResource("../view/loginbackground.fxml"));

			    root = loader.load();
				Scene scene = new Scene(root);
				stage.initStyle(StageStyle.DECORATED);
				stage.setScene(scene);
				stage.setTitle("用户登录");
				stage.setResizable(true);// 能最大化
				stage.getIcons().add(new Image(getClass().getResourceAsStream("../view/logo.png")));
				stage.centerOnScreen();// 屏幕正中间
				stage.setOnCloseRequest(e -> System.exit(0));// 完全退出
				stage.show();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public static void main(String[] args) {
			m2=new ButtonBGM();
			launch(args);
		}

}
