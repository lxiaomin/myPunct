package com.punct.controlers;


public class ChessBoard {

	public static ChessPoints[][][] chessPoints;
	/*棋盘初始化时候，按照XY坐标轴定位每个下棋点*/
	public ChessBoard() {
	    chessPoints=new ChessPoints[3][17][];

		chessPoints[0][0]=new ChessPoints[9];
		chessPoints[0][1]=new ChessPoints[10];
		chessPoints[0][2]=new ChessPoints[11];
		chessPoints[0][3]=new ChessPoints[12];
		chessPoints[0][4]=new ChessPoints[13];
		chessPoints[0][5]=new ChessPoints[14];
		chessPoints[0][6]=new ChessPoints[15];
		chessPoints[0][7]=new ChessPoints[16];
		chessPoints[0][8]=new ChessPoints[17];
		chessPoints[0][9]=new ChessPoints[16];
		chessPoints[0][10]=new ChessPoints[15];
		chessPoints[0][11]=new ChessPoints[14];
		chessPoints[0][12]=new ChessPoints[13];
		chessPoints[0][13]=new ChessPoints[12];
		chessPoints[0][14]=new ChessPoints[11];
		chessPoints[0][15]=new ChessPoints[10];
		chessPoints[0][16]=new ChessPoints[9];

		chessPoints[1][0]=new ChessPoints[9];
		chessPoints[1][1]=new ChessPoints[10];
		chessPoints[1][2]=new ChessPoints[11];
		chessPoints[1][3]=new ChessPoints[12];
		chessPoints[1][4]=new ChessPoints[13];
		chessPoints[1][5]=new ChessPoints[14];
		chessPoints[1][6]=new ChessPoints[15];
		chessPoints[1][7]=new ChessPoints[16];
		chessPoints[1][8]=new ChessPoints[17];
		chessPoints[1][9]=new ChessPoints[16];
		chessPoints[1][10]=new ChessPoints[15];
		chessPoints[1][11]=new ChessPoints[14];
		chessPoints[1][12]=new ChessPoints[13];
		chessPoints[1][13]=new ChessPoints[12];
		chessPoints[1][14]=new ChessPoints[11];
		chessPoints[1][15]=new ChessPoints[10];
		chessPoints[1][16]=new ChessPoints[9];

		chessPoints[2][0]=new ChessPoints[9];
		chessPoints[2][1]=new ChessPoints[10];
		chessPoints[2][2]=new ChessPoints[11];
		chessPoints[2][3]=new ChessPoints[12];
		chessPoints[2][4]=new ChessPoints[13];
		chessPoints[2][5]=new ChessPoints[14];
		chessPoints[2][6]=new ChessPoints[15];
		chessPoints[2][7]=new ChessPoints[16];
		chessPoints[2][8]=new ChessPoints[17];
		chessPoints[2][9]=new ChessPoints[16];
		chessPoints[2][10]=new ChessPoints[15];
		chessPoints[2][11]=new ChessPoints[14];
		chessPoints[2][12]=new ChessPoints[13];
		chessPoints[2][13]=new ChessPoints[12];
		chessPoints[2][14]=new ChessPoints[11];
		chessPoints[2][15]=new ChessPoints[10];
		chessPoints[2][16]=new ChessPoints[9];
		InitChessBoard(chessPoints);
	}

	private void InitChessBoard(ChessPoints[][][] chessPoints2) {

		for(int k=0;k<3;k++)
		for(int i=0;i<17;i++)
		{
			for(int j=0;j<chessPoints2[k][i].length;j++)
			{
				chessPoints2[k][i][j]=new ChessPoints();
				chessPoints2[k][i][j].setDatax(i);
				chessPoints2[k][i][j].setDatay(j);
			}
		}
	}


}
