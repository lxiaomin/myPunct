package com.punct.controlers;

public class ChessPoints {

	private int datax; //数据横坐标
	private int datay; //数据中坐标
	private int coordinateX; //相对容器横坐标
	private int coordinateY; //相对容器纵坐标
	private boolean enable;
	public Node chessNode;
	public int dir;
	ChessPoints next;
	public ChessPoints()
	{
		datax=0;
		datay=0;
		coordinateX=0;
		coordinateX=0;
		enable=false;
		next=null;
	}

	public int getDatax() {
		return datax;
	}

	public void setDatax(int datax) {
		this.datax = datax;
	}

	public int getDatay() {
		return datay;
	}

	public void setDatay(int datay) {
		this.datay = datay;
	}

	public int getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}

	public int getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}



}
