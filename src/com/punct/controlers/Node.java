package com.punct.controlers;

public class Node {

	private int datax; //数据横坐标
	private int datay; //数据中坐标
	private int coordinateX; //相对容器横坐标
	private int coordinateY; //相对容器纵坐标
	public int dir;
	public String chessName="";
	public int type;
	public int color;
	public int index;
	public int level;
	public Node()
	{
		datax=-1;
		datay=-1;
		coordinateX=-1;
		coordinateX=-1;
		dir=0;
		chessName="";
		type=-1;
		color=0;
		index=0;
		level=0;
	}

	public int getDatax() {
		return datax;
	}

	public void setDatax(int datax) {
		this.datax = datax;
	}

	public int getDatay() {
		return datay;
	}

	public void setDatay(int datay) {
		this.datay = datay;
	}

	public int getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}

	public int getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
	}

	public void finalize() throws Throwable{

		;
	}



}
