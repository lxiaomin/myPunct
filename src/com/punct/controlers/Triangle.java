package com.punct.controlers;

public class Triangle {

	int order;
	public Node center;
	public Node left;
	public Node right;

	/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
	private int level;

	/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
	private int dir;

	/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
	int color;

	public Triangle() {
		center=new Node();
		left=new Node();
		right=new Node();

	}

	/* 初始化每个三角形的棋子所占用的坐标 */
	public Triangle(int centerX, int centerY) {
		level = 0;
		dir = 0;
		center.setDatax(centerX);
		center.setDatay(centerY);
		// left.setDatax(centerX);
		// left.setDatay(centerY);
		// right.setDatax(centerX);
		// right.setDatay(centerY);

	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}
}