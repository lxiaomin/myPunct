package com.punct.controlers;

import com.punct.move.ChessesRotate;

public class Chesses {

	int order;
	public Node[] points;// 默认node[0]是这个棋子的关键点，只要能找到这个棋子就能找到它的位置

	/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
	private int level;

	/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
	private int dir;

	/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
	int color;
	public Chesses() {;}



	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getDir() {
		return dir;
	}

	public void setDir(int dir) {
		this.dir = dir;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}


//	public class Triangle {
//
//		int order;
//		Node center;
//		Node left;
//		Node right;
//
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//
//		public Triangle() {
//			;
//		}
//
//		/* 初始化每个三角形的棋子所占用的坐标 */
//		public Triangle(int centerX, int centerY) {
//			level = 0;
//			dir = 0;
//			center.setDatax(centerX);
//			center.setDatay(centerY);
//			// left.setDatax(centerX);
//			// left.setDatay(centerY);
//			// right.setDatax(centerX);
//			// right.setDatay(centerY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//	}

//	public class LineTop {
//
//		int order;
//		Node top;
//		Node center;
//		Node buttom;
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		public LineTop() {
//			;
//		}
//
//		public LineTop(int topX, int topY) {
//			level = 0;
//			dir = 0;
//			top.setDatax(topX);
//			top.setDatay(topY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//
//	}

//	public class LineCenter {
//
//		int order;
//		Node top;
//		Node center;
//		Node buttom;
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		public LineCenter() {
//			;
//		}
//
//		public LineCenter(int topX, int topY) {
//			level = 0;
//			dir = 0;
//			top.setDatax(topX);
//			top.setDatay(topY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//
//	}

//	public class ArchTop {
//
//		int order;
//		Node top;
//		Node center;
//		Node buttom;
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		public ArchTop() {
//			;
//		}
//
//		public ArchTop(int topX, int topY) {
//			level = 0;
//			dir = 0;
//			top.setDatax(topX);
//			top.setDatay(topY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//
//	}

//	public class ArchCenter {
//
//		int order;
//		Node top;
//		Node center;
//		Node buttom;
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		public ArchCenter() {
//			;
//		}
//
//		public ArchCenter(int topX, int topY) {
//			level = 0;
//			dir = 0;
//			top.setDatax(topX);
//			top.setDatay(topY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//
//	}

//	public class ArchBottom {
//
//		int order;
//		Node top;
//		Node center;
//		Node buttom;
//		/* 变量说明，颜色：正数代表白色玩家，负数代表黑色玩家 */
//		int color;
//		/* 变量说明,level:层数，意思是现在该棋子所在的层数。没放置的时候在第0层。开始放置的时候都是第一层，棋子移动可达到第二层 */
//		private int level;
//		/* 变量说明，dir，方向。意思是棋子在没有障碍的时候有6个方向可以旋转。初始没有放置的时候，方向是0。 */
//		private int dir;
//
//		public ArchBottom() {
//			;
//		}
//
//		public ArchBottom(int topX, int topY) {
//			level = 0;
//			dir = 0;
//			top.setDatax(topX);
//			top.setDatay(topY);
//
//		}
//
//		public int getOrder() {
//			return order;
//		}
//
//		public void setOrder(int order) {
//			this.order = order;
//		}
//
//		public int getColor() {
//			return color;
//		}
//
//		public void setColor(int color) {
//			this.color = color;
//		}
//
//		public int getLevel() {
//			return level;
//		}
//
//		public void setLevel(int level) {
//			this.level = level;
//		}
//
//		public int getDir() {
//			return dir;
//		}
//
//		public void setDir(int dir) {
//			this.dir = dir;
//		}
//
//	}

}
