package com.punct.bgm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class BackgroundMusic {

	static public playThread music = new playThread();
	static public Thread musicThread = new Thread(music);
	static Calendar seed = Calendar.getInstance();
	static Random rd = new Random(seed.getTimeInMillis());
	public static int shangyishou = 1;
	public static int state = 0;
	public static int i = 1;
	public static int k[] = new int[300];
	public static String ImportMusic = "-1"; // 用这个来判断是不是用户导入的音乐
	static ArrayList<String> bgmURL = new ArrayList<String>() {
		{
			add("res/Musics/BGM/大家族.wav");
			add("res/Musics/BGM/二人距离.wav");
			add("res/Musics/BGM/风.wav");
			add("res/Musics/BGM/女子十二乐坊.wav");
			add("res/Musics/BGM/森林狂想曲.wav");
			add("res/Musics/BGM/夏恋.wav");
			add("res/Musics/BGM/LuvLetter.wav");
			add("res/Musics/BGM/Memory.wav");
			add("res/Musics/BGM/Summer.wav");
		}
	};;

	public static class playThread implements Runnable {
		@Override
		public void run() {
			while (true) {
				if (ImportMusic.equals("-1")) {
					k[shangyishou] = i;
					if (state == 0) { // 正常时候随机循环播放
						i = k[shangyishou];
						while (i == k[shangyishou])// 这样点击下一首的时候就不会与上一首重复
							i = rd.nextInt(bgmURL.size());
						String URL = bgmURL.get(i);
						play(URL);

					} else if (state == 1) // 1的状态时。播放上一首
					{
						String URL = bgmURL.get(k[shangyishou - 1]);
						play2(URL);

						// System.out.println(shangyishou);
					} else //
					{
						i = k[shangyishou];
						while (i == k[shangyishou])// 这样点击下一首的时候就不会与上一首重复
							i = rd.nextInt(bgmURL.size()); // 下一首
						String URL = bgmURL.get(i);
						play3(URL);

					}

					shangyishou++;
				}

				else {
					// System.out.println(ImportMusic);
					play4(ImportMusic);
					ImportMusic = "-1";
					state = 0;

				}
			}

		}

		public static void setMusics(String URL) {
			;

		}

		public void play(String URL) {
			File file;
			AudioInputStream audio;
			AudioFormat format;
			SourceDataLine auline = null;
			DataLine.Info info;
			try {
				//读取文件
				file = new File(URL);
				audio = AudioSystem.getAudioInputStream(file);
				format = audio.getFormat();
				info = new DataLine.Info(SourceDataLine.class, format);
				auline = (SourceDataLine) AudioSystem.getLine(info);
				auline.open(format);
				auline.start();
				int nBytesRead = 0;
				byte[] abData = new byte[1024];// 一次只读1024，方便暂停
				//播放音乐，state判断现在控制的状态
				while (nBytesRead != -1 && state == 0) {
					nBytesRead = audio.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						auline.write(abData, 0, nBytesRead);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				e.printStackTrace();
			} finally {
				//关闭
				auline.drain();
				auline.close();
			}

		}

		public void play2(String URL) {
			File file;
			AudioInputStream audio;
			AudioFormat format;
			SourceDataLine auline = null;
			DataLine.Info info;
			try {

				file = new File(URL);
				audio = AudioSystem.getAudioInputStream(file);
				format = audio.getFormat();
				info = new DataLine.Info(SourceDataLine.class, format);
				auline = (SourceDataLine) AudioSystem.getLine(info);
				auline.open(format);
				auline.start();
				int nBytesRead = 0;
				byte[] abData = new byte[1024];
				while (nBytesRead != -1 && state == 1) {
					nBytesRead = audio.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						auline.write(abData, 0, nBytesRead);
					}
				}
			} catch (IOException e) {
				// System.out.println(e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				auline.drain();
				auline.close();
			}

		}

		public void play3(String URL) {
			File file;
			AudioInputStream audio;
			AudioFormat format;
			SourceDataLine auline = null;
			DataLine.Info info;
			try {

				file = new File(URL);
				audio = AudioSystem.getAudioInputStream(file);
				format = audio.getFormat();
				info = new DataLine.Info(SourceDataLine.class, format);
				auline = (SourceDataLine) AudioSystem.getLine(info);
				auline.open(format);
				auline.start();
				int nBytesRead = 0;
				byte[] abData = new byte[1024];
				while (nBytesRead != -1 && state == 2) {
					nBytesRead = audio.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						auline.write(abData, 0, nBytesRead);
					}
				}
			} catch (IOException e) {
				// System.out.println(e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				auline.drain();
				auline.close();
			}

		}

		public void play4(String URL) {
			File file;
			AudioInputStream audio;
			AudioFormat format;
			SourceDataLine auline = null;
			DataLine.Info info;
			try {

				file = new File(URL);
				audio = AudioSystem.getAudioInputStream(file);
				format = audio.getFormat();
				info = new DataLine.Info(SourceDataLine.class, format);
				auline = (SourceDataLine) AudioSystem.getLine(info);
				auline.open(format);
				auline.start();
				int nBytesRead = 0;
				byte[] abData = new byte[1024];
				while (nBytesRead != -1 && state == 4) {
					nBytesRead = audio.read(abData, 0, abData.length);
					if (nBytesRead >= 0) {
						auline.write(abData, 0, nBytesRead);
					}
				}
			} catch (IOException e) {
				// System.out.println(e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				auline.drain();
				auline.close();
			}

		}

	}

	public void startPlay() {

		musicThread.start();

	}

}
