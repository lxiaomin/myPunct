package com.punct.netmode.common;

import com.netmode.Wan.Server;
import com.punct.netmode.Lan.Client;

public class Room {

	public static Server server;
	public static Client client;
	public String Player1,Player2,Player1IP,Player2IP,state;
	public String getPlayer1() {
		return Player1;
	}
	public void setPlayer1(String player1) {
		Player1 = player1;
	}
	public String getPlayer2() {
		return Player2;
	}
	public void setPlayer2(String player2) {
		Player2 = player2;
	}
	public String getPlayer1IP() {
		return Player1IP;
	}
	public void setPlayer1IP(String player1ip) {
		Player1IP = player1ip;
	}
	public String getPlayer2IP() {
		return Player2IP;
	}
	public void setPlayer2IP(String player2ip) {
		Player2IP = player2ip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public long getRoomID() {
		return RoomID;
	}
	public void setRoomID(long roomID) {
		RoomID = roomID;
	}
	private long RoomID;
	public Room() {

	}

}
