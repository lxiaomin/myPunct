package com.punct.netmode.common;

import com.punct.logic.CommonChessBoard;
import com.punct.logic.GameMenu;
import com.punct.logic.Player;
import com.punct.logic.Role;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GiveUp extends Application {
	   @FXML
	    private Button cancelButton;

	    @FXML
	    private Label detailsLabel;

	    @FXML
	    private HBox actionParent;

	    @FXML
	    private Button okButton;

	    @FXML
	    private HBox okParent;

	    @FXML
	    private Label messageLabel;

	    @FXML
	    void cancelGiveUp(ActionEvent event) {

	    	((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏
	    }

	    @FXML
	    void sureGiveUp(ActionEvent event) {
	    	Platform.runLater(new Runnable() {
				public void run() {
					try {
						if(CommonChessBoard.IsServer)
							CommonChessBoard.getThisserver().CloseServer();
						else
							CommonChessBoard.getThisclient().closeClient();
						new GameMenu().start(new Stage());
						((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});


	    }

	public GiveUp() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		launch(args);

	}
	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(GiveUp.class.getResource("../../view/giveUpcomfirm.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("认输确认");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间

			stage.getIcons().add(new Image(getClass().getResourceAsStream("../../view/logo.png")));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
