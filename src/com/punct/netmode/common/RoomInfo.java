package com.punct.netmode.common;

public class RoomInfo {

	String roomName,master,nowNum,players,state;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public String getNowNum() {
		return nowNum;
	}

	public void setNowNum(String nowNum) {
		this.nowNum = nowNum;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public RoomInfo() {
		// TODO Auto-generated constructor stub
	}

	public RoomInfo(String roomName, String master, String nowNum, String players, String state) {
		super();
		this.roomName = roomName;
		this.master = master;
		this.nowNum = nowNum;
		this.players = players;
		this.state = state;
	}

}
