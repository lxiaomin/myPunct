package com.punct.netmode.common;

import java.io.Serializable;

public class Message  implements Serializable {
	/*
	 * ID变量说明：
	 * 1、指的是用户选择下新的棋子完毕
	 * 2、指的是用户移动棋子完毕
	 *
	 * */

	private int messageID;
	private boolean valueBoolean;
	public String ValueString;
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public boolean isValueBoolean() {
		return valueBoolean;
	}
	public void setValueBoolean(boolean valueBoolean) {
		this.valueBoolean = valueBoolean;
	}
	public String getValueString() {
		return ValueString;
	}
	public void setValueString(String valueString) {
		ValueString = valueString;
	}

	public Message(int messageID, boolean valueBoolean) {
		super();
		this.messageID = messageID;
		this.valueBoolean = valueBoolean;
	}


	public Message(int messageID, String valueString) {
		super();
		this.messageID = messageID;
		ValueString = valueString;
	}
	public Message() {
		// TODO Auto-generated constructor stub
	}

}
