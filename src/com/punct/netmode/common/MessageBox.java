package com.punct.netmode.common;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MessageBox extends Application{

	  @FXML
	    private Label detailsLabel;

	    @FXML
	    private HBox actionParent;

	    @FXML
	    private Button okButton;

	    @FXML
	    private HBox okParent;

	    @FXML
	    private Label messageLabel;

	    @FXML
	    void OKevent(ActionEvent event) {
	    	((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏
	    }

	public static String title,message="";

	public MessageBox() {

	}
	public MessageBox(String titleString,String messageString) {
		this.title=titleString;
		this.message=messageString;

	}
	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {
	messageLabel.setText(title);
	detailsLabel.setText(message);

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(MessageBox.class.getResource("../../view/MessageBox.fxml"));

			Parent root = loader.load();
			Scene scene = new Scene(root);
			stage.initStyle(StageStyle.DECORATED);
			stage.setScene(scene);
			stage.setTitle("提示");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间

			stage.getIcons().add(new Image(getClass().getResourceAsStream("../../view/logo.png")));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
