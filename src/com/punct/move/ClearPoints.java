package com.punct.move;

import com.punct.controlers.ChessBoardModel;
import com.punct.logic.StandaloneBegin;

public class ClearPoints {

	public static int ClearTempDir=0;
	public static int ClearXYdata[]=new int [2];
	public static int chessType;
	public static int level;
	public ClearPoints() {

	}



	public static void clear(int NowchessDir,int ClearXYdata[])
	{
		/*进行清除操作前，先备份一份原来的数据*/
		save();
		ClearPoints.ClearXYdata=ClearXYdata;
		switch (chessType) {
		case 0:
			type0(NowchessDir);
			break;
		case 1:
			type1(NowchessDir);
			break;
		case 2:
			type2(NowchessDir);
			break;
		case 3:
			type3(NowchessDir);
			break;
		case 4:
			type4(NowchessDir);
			break;
		case 5:
			type5(NowchessDir);
			break;
		default:
			break;
		}

	}


	private static void save() {


	}



	private static void type0(int NowchessDir) {
		switch (NowchessDir) {
		case 0://0是竖直向下的，然后剩下的12345是顺时针方向的几个剩下的方向
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode=null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-2].chessNode=null;

			break;
		case 1:
			if(ClearXYdata[0]<9)	{StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]-2].chessNode=null;
			}else if(ClearXYdata[0]==9) {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]-1].chessNode=null;
			}
			else {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]].chessNode=null;
			}
			break;
		case 2:
			if(ClearXYdata[0]<9)	{StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]].chessNode=null;
			}
			else if(ClearXYdata[0]==9) {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]+1].chessNode=null;
			}
			else {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]+2].chessNode=null;
			}
			break;
		case 3:
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode=null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+2].chessNode=null;

			break;
		case 4:
			if(ClearXYdata[0]<7)	{StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]+2].chessNode=null;}
			else if(ClearXYdata[0]==7) {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]+1].chessNode=null;
			}
			else {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]].chessNode=null;
			}break;
		case 5:
			if(ClearXYdata[0]<7)	{StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]].chessNode=null;
			}
			else if(ClearXYdata[0]==7) {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]-1].chessNode=null;
			}
			else {StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]-2].chessNode=null;
			}break;
		default:
			break;

		}

	}


	private static void type1(int NowchessDir) {
		switch (NowchessDir)
		{
		case 0:StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;StandaloneBegin.chessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
		break;
		case 1:
		ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

		}
		break;
		case 2:
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			break;
		default:break;
		}

	}



	private static void type2(int NowchessDir) {

	ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
	switch (NowchessDir) {
	case 0:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
					ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
					ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
					ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

		}
		break;
	case 1:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;

		}
		break;
	case 2:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode  = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode =null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

		}
		break;
	case 3:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

		}
		break;
	case 4:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

		}
		break;
	case 5:
		if(ClearXYdata[0]<8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

		}
		else if(ClearXYdata[0]==8)
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

		}
		else
		{
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
			ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

		}
		break;
	default:
		break;
	}
	}


	private static void type3(int NowchessDir) {
		ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
		switch (NowchessDir) {
		case 0:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-2].chessNode =  null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-2].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode =  null;

			}
			break;
		case 1:
			if(ClearXYdata[0]<9)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]-1].chessNode =  null;

			}
			else if(ClearXYdata[0]==9)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
					ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]+1].chessNode =  null;

			}
			break;
		case 2:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode =  null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+2].chessNode =  null;

			}
			break;
		case 3:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+2].chessNode =  null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode =  null;

			}
			break;
		case 4:
			if(ClearXYdata[0]<7)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]+1].chessNode =  null;

			}
			else if(ClearXYdata[0]==7)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]-1].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]-1].chessNode =  null;

			}
			break;
		case 5:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode =  null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-2].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-2].chessNode =  null;

			}

			break;
		default:
			break;
		}

	}



	private static void type4(int NowchessDir) {
		ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;
		switch (NowchessDir) {
		case 0:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

			}


//			if(ClearXYdata[0]<8)
//			{
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
//
//			}
//			else if(ClearXYdata[0]==8)
//			{
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode =  null;
//
//			}
//			else
//			{
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
//				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode =  null;
//
//			}
			break;
		case 1:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;
			}
			break;
		case 2:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;

			}
			break;
		case 3:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;

			}
			break;
		case 4:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode =  null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode =  null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode =  null;
			}
			break;

		case 5:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			break;

		default:
			break;
		}

	}



	private static void type5(int NowchessDir) {
		ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]].chessNode=null;

		switch (NowchessDir) {
		case 0:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+2].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode =null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode =null;

			}
			break;
		case 1:
			if(ClearXYdata[0]<7)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]+1].chessNode = null;

			}
			else if(ClearXYdata[0]==7)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+2][ClearXYdata[1]-1].chessNode = null;

			}
			break;
		case 2:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]+1][ClearXYdata[1]-2].chessNode = null;

			}
			break;
		case 3:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-2].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-2].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
			}
			break;
		case 4:
			if(ClearXYdata[0]<=8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]-1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]-1].chessNode =null;

			}
			else if(ClearXYdata[0]==9)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]].chessNode =null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-2][ClearXYdata[1]+1].chessNode =null;

			}
			break;
		case 5:
			if(ClearXYdata[0]<8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;

			}
			else if(ClearXYdata[0]==8)
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;

			}
			else
			{
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+1].chessNode = null;
				ChessBoardModel.chessBoard.chessPoints[level][ClearXYdata[0]-1][ClearXYdata[1]+2].chessNode = null;

			}
			break;
		default:
			break;
		}

	}





}
