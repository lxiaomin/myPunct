package com.punct.move;

import com.punct.controlers.ChessBoardModel;
import com.punct.logic.StandaloneBegin;

public class JudgeIsCovered {

	public JudgeIsCovered() {

	}



	public static boolean IsCovered(int i,int j,int level,int NowchessDir,int chessType)
	{

		switch (chessType) {
		case 0:
			if(type0(i,j,level,NowchessDir))
				return true;
			break;
		case 1:
			if(type1(i,j,level,NowchessDir))
			return true;break;
		case 2:
			if(type2(i,j,level,NowchessDir))
			return true;break;
		case 3:
			if(type3(i,j,level,NowchessDir))
			return true;break;
		case 4:
			if(type4(i,j,level,NowchessDir))
			return true;break;
		case 5:
			if(type5(i,j,level,NowchessDir))
			return true;break;
		default:
			break;
		}
		return false;

	}

	private static boolean type0(int i, int j, int level, int NowchessDir) {
		switch (NowchessDir) {
		case 0://0是竖直向下的，然后剩下的12345是顺时针方向的几个剩下的方向
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode!=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-2].chessNode!=null) return true;

			break;
		case 1:
			if(i<9)	{if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j-2].chessNode!=null) return true;
			}else if(i==9) {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j-1].chessNode!=null) return true;
			}
			else {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j].chessNode!=null) return true;
			}
			break;
		case 2:
			if(i<9)	{if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j].chessNode!=null) return true;
			}
			else if(i==9) {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j+1].chessNode!=null) return true;
			}
			else {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j+2].chessNode!=null) return true;
			}
			break;
		case 3:
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode!=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+2].chessNode!=null) return true;

			break;
		case 4:
			if(i<7)	{if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j+2].chessNode!=null) return true;}
			else if(i==7) {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j+1].chessNode!=null) return true;
			}
			else {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j].chessNode!=null) return true;
			}break;
		case 5:
			if(i<7)	{if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j].chessNode!=null) return true;
			}
			else if(i==7) {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j-1].chessNode!=null) return true;
			}
			else {if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j-2].chessNode!=null) return true;
			}break;
		default:
			break;

		}
		return false;

	}


	private static boolean type1(int i, int j, int level, int NowchessDir) {
		switch (NowchessDir)
		{
		case 0:if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
		break;
		case 1:
		if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

		}
		break;
		case 2:
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			break;
		default:break;
		}
		return false;

	}



	private static boolean type2(int i, int j, int level, int NowchessDir) {

	if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
	switch (NowchessDir) {
	case 0:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
					if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
					if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
					if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

		}
		break;
	case 1:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

		}
		break;
	case 2:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode  !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

		}
		break;
	case 3:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

		}
		break;
	case 4:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

		}
		break;
	case 5:
		if(i<8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

		}
		else if(i==8)
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

		}
		else
		{
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
			if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

		}
		break;
	default:
		break;
	}
	return false;
	}


	private static boolean type3(int i, int j, int level, int NowchessDir) {
		if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
		switch (NowchessDir) {
		case 0:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-2].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-2].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

			}
			break;
		case 1:
			if(i<9)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j-1].chessNode !=null) return true;

			}
			else if(i==9)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
					if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j+1].chessNode !=null) return true;

			}
			break;
		case 2:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+2].chessNode !=null) return true;

			}
			break;
		case 3:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+2].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			break;
		case 4:
			if(i<7)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j+1].chessNode !=null) return true;

			}
			else if(i==7)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j-1].chessNode !=null) return true;

			}
			break;
		case 5:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-2].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-2].chessNode !=null) return true;

			}

			break;
		default:
			break;
		}
		return false;

	}



	private static boolean type4(int i, int j, int level, int NowchessDir) {
		if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;
		switch (NowchessDir) {
		case 0:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

			}


//			if(i<8)
//			{
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
//
//			}
//			else if(i==8)
//			{
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
//
//			}
//			else
//			{
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
//				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
//
//			}
			break;
		case 1:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;
			}
			break;
		case 2:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

			}
			break;
		case 3:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;

			}
			break;
		case 4:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
			}
			break;

		case 5:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			break;

		default:
			break;
		}
		return false;

	}



	private static boolean type5(int i, int j, int level, int NowchessDir) {
		if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j].chessNode!=null) return true;

		switch (NowchessDir) {
		case 0:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+2].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;

			}
			break;
		case 1:
			if(i<7)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j+1].chessNode !=null) return true;

			}
			else if(i==7)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+2][j-1].chessNode !=null) return true;

			}
			break;
		case 2:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i+1][j-2].chessNode !=null) return true;

			}
			break;
		case 3:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-2].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-2].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
			}
			break;
		case 4:
			if(i<=8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j-1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j-1].chessNode !=null) return true;

			}
			else if(i==9)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-2][j+1].chessNode !=null) return true;

			}
			break;
		case 5:
			if(i<8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

			}
			else if(i==8)
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;

			}
			else
			{
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+1].chessNode !=null) return true;
				if(	ChessBoardModel.chessBoard.chessPoints[level+1][i-1][j+2].chessNode !=null) return true;

			}
			break;
		default:
			break;
		}
		return false;

	}



}
