package com.punct.move;

import com.punct.controlers.ChessBoardModel;


public class ChessRotatePositionProvider {

	public static String result;

	public ChessRotatePositionProvider() {
		// TODO Auto-generated constructor stub
	}

	/* 找出所有点中符合放该棋子的所有下棋点。将下棋点的ID返回。 */
	public static String getDiraction(int chess,int i,int j,int level) {
		result = "";
		switch (chess) {
		case 0:IsLineTopAvaliable(i, j, level);
			break;
		case 1:IsLineCenterAvaliable(i, j, level);

		break;
		case 2:IsLineTriangleAvaliable(i, j, level);

		break;
		case 3:IsArchTopAvaliable(i, j, level);

		break;
		case 4:IsArchCenterAvaliable(i, j, level);

		break;
		case 5:IsArchBottomAvaliable(i, j, level);

		break;
		default:
			break;
		}
		return result;

	}


	private static void IsArchBottomAvaliable(int i, int j,int level) {
		// 判断6个方向哪个能走
		if(filter_5_0(i, j))
		{
			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-2].chessNode == null)
				{
					result+="3";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-2].chessNode == null)
				{
					result+="3";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
				{
					result+="3";
				}
			}

		}
		if(filter_5_1(i, j))
		{

			if(i<=8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j-1].chessNode == null)
				{
					result+="4";
				}
			}
			else if(i==9)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j].chessNode == null)
				{
					result+="4";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j+1].chessNode == null)
				{
					result+="4";
				}
			}

		}
		if(filter_5_2(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
				{
					result+="5";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
				{
					result+="5";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+2].chessNode == null)
				{
					result+="5";
				}
			}

		}
		if(filter_5_3(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+2].chessNode == null)
				{
					result+="0";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
				{
					result+="0";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
				{
					result+="0";
				}
			}

		}
		if(filter_5_4(i, j))
		{

			if(i<7)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j+1].chessNode == null)
				{
					result+="1";
				}
			}
			else if(i==7)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j].chessNode == null)
				{
					result+="1";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j-1].chessNode == null)
				{
					result+="1";
				}
			}

		}
		if(filter_5_5(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
				{
					result+="2";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
				{
					result+="2";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-2].chessNode == null)
				{
					result+="2";
				}
			}

		}
	return;
	}

	private static void IsArchCenterAvaliable(int i, int j,int level) {
		// 判断6个方向哪个能走
				if(filter_4_0(i, j))
				{
					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="1";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="1";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null && ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="1";
						}
					}

				}
				if(filter_4_1(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="2";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="2";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
						{
							result+="2";
						}
					}

				}
				if(filter_4_2(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="3";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="3";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="3";
						}
					}

				}
				if(filter_4_3(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
						{
							result+="4";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="4";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="4";
						}
					}

				}
				if(filter_4_4(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="5";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="5";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="5";
						}
					}

				}
				if(filter_4_5(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
						{
							result+="0";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="0";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="0";
						}
					}

				}
			return;
	}

	private static void IsArchTopAvaliable(int i, int j,int level) {
		// 判断6个方向哪个能走
		if(filter_3_0(i, j))
		{
			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-2].chessNode == null)
				{
					result+="0";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-2].chessNode == null)
				{
					result+="0";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
				{
					result+="0";
				}
			}

		}
		if(filter_3_1(i, j))
		{

			if(i<9)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j-1].chessNode == null)
				{
					result+="1";
				}
			}
			else if(i==9)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j].chessNode == null)
				{
					result+="1";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-2][j+1].chessNode == null)
				{
					result+="1";
				}
			}

		}
		if(filter_3_2(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
				{
					result+="2";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
				{
					result+="2";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i-1][j+2].chessNode == null)
				{
					result+="2";
				}
			}

		}
		if(filter_3_3(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+2].chessNode == null)
				{
					result+="3";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
				{
					result+="3";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
				{
					result+="3";
				}
			}

		}
		if(filter_3_4(i, j))
		{

			if(i<7)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j+1].chessNode == null)
				{
					result+="4";
				}
			}
			else if(i==7)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j-1].chessNode == null)
				{
					result+="4";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+2][j-1].chessNode == null)
				{
					result+="4";
				}
			}

		}
		if(filter_3_5(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
				{
					result+="5";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-2].chessNode == null)
				{
					result+="5";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-2].chessNode == null)
				{
					result+="5";
				}
			}

		}
	return;

	}

	private static void IsLineTriangleAvaliable(int i, int j,int level) {

		// 判断6个方向哪个能走
				if(filter_2_0(i, j))
				{
					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="5";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="5";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="5";
						}
					}

				}
				if(filter_2_1(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="0";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null)
						{
							result+="0";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="0";
						}
					}

				}
				if(filter_2_2(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="1";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null)
						{
							result+="1";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null)
						{
							result+="1";
						}
					}

				}
				if(filter_2_3(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
						{
							result+="2";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="2";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="2";
						}
					}

				}
				if(filter_2_4(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="3";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="3";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="3";
						}
					}

				}
				if(filter_2_5(i, j))
				{

					if(i<8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
						{
							result+="4";
						}
					}
					else if(i==8)
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="4";
						}
					}
					else
					{
						if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
								ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
						{
							result+="4";
						}
					}

				}
			return;

	}

	private static void IsLineCenterAvaliable(int i, int j,int level) {
		// 判断6个方向哪个能走
		if(filter_1_0(i, j,level))
		{
			if(ChessBoardModel.chessBoard.chessPoints[level][i][j-1].chessNode == null &&
					ChessBoardModel.chessBoard.chessPoints[level][i][j+1].chessNode == null)
			{
				result+="0";
			}
		}
		if(filter_1_1(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j+1].chessNode == null)
				{
					result+="1";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j-1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
				{
					result+="1";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
				{
					result+="1";
				}
			}

		}
		if(filter_1_2(i, j))
		{

			if(i<8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j].chessNode == null)
				{
					result+="2";
				}
			}
			else if(i==8)
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
				{
					result+="2";
				}
			}
			else
			{
				if(ChessBoardModel.chessBoard.chessPoints[level][i-1][j+1].chessNode == null &&
						ChessBoardModel.chessBoard.chessPoints[level][i+1][j-1].chessNode == null)
				{
					result+="2";
				}
			}

		}
		return;
	}





	private static void IsLineTopAvaliable(int i, int j,int level) {
		/* 判断直线型的六个方向哪个能走的 */
		// if(ChessBoardModel.chessBoard.chessPoints[level][i][j].chessNode!=null )
		if (((j - 2 >= 0 && (i != 0 || i != 16 || i != 8)) || (j - 2 > 0 && (i == 0 || i == 16 || i != 8))) && filter_0_0(i,j)) {
			if (ChessBoardModel.chessBoard.chessPoints[level][i][j - 1].chessNode == null
					&& ChessBoardModel.chessBoard.chessPoints[level][i][j - 2].chessNode == null) {
				result += "0";
			}

		}
		if (filter_0_4(i, j)) {
			if (i < 9)
			{
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j - 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j - 2].chessNode == null)
					result += "1";
			}
			if (i == 9)
			{
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j - 1].chessNode == null)
					result += "1";
			}
			if(i>9) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j].chessNode == null)
					result += "1";
			}
		}
		if (filter_0_5(i, j)) {
			if (i < 9) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j].chessNode == null)
					result += "2";
			}
			if (i == 9) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j + 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j + 1].chessNode == null)
					result += "2";
			}
			if(i>9)
			{
				if (ChessBoardModel.chessBoard.chessPoints[level][i - 1][j + 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i - 2][j + 2].chessNode == null)
					result += "2";
			}
		}
		if ((((j + 2 <= ChessBoardModel.chessBoard.chessPoints[level][i].length - 1) && (i != 0 || i != 16 || i != 8))
				|| ((j + 2 < ChessBoardModel.chessBoard.chessPoints[level][i].length - 1) && (i == 0 || i == 16 || i == 8)) ) && filter_0_1(i,j)) {
			if (ChessBoardModel.chessBoard.chessPoints[level][i][j + 2].chessNode == null
					&& ChessBoardModel.chessBoard.chessPoints[level][i][j + 1].chessNode == null) {
				result += "3";
			} else {

			}
		}

		if (filter_0_6(i, j)) {
			if (i < 7) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j + 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j + 2].chessNode == null) {
					result += "4";
				}
			} else if (i == 7) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j + 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j + 1].chessNode == null) {
					result += "4";
				}
			} else {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j].chessNode == null) {
					result += "4";
				}
			}

		}
		if (filter_0_3(i, j)) {
			if (i < 7) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j].chessNode == null) {
					result += "5";
				}
			} else if (i == 7) {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j - 1].chessNode == null) {
					result += "5";
				}
			} else {
				if (ChessBoardModel.chessBoard.chessPoints[level][i + 1][j - 1].chessNode == null
						&& ChessBoardModel.chessBoard.chessPoints[level][i + 2][j - 2].chessNode == null) {
					result += "5";
				}
			}

		}

	}

	private static boolean filter_0_0(int i, int j) {
		if(i==6 && j==10) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==11) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==12) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==10) return false;
		else if(i==10 && j==9) return false;

		return true;
}

	private static boolean filter_0_1(int i, int j) {
		if(i==6 && j==5) return false;
		else if(i==6 && j==4) return false;
		else if(i==7 && j==5) return false;
		else if(i==7 && j==4) return false;
		else if(i==8 && j==5) return false;
		else if(i==8 && j==4) return false;
		else if(i==9 && j==5) return false;
		else if(i==9 && j==4) return false;
		else if(i==10 && j==5) return false;
		else if(i==10 && j==4) return false;


		return true;
}
	/*过滤第一种棋子的第三种走法的不可用点*/
	private static boolean filter_0_3(int i, int j) {

		if(i==15 || i==16) return false;
		else if(i==6 && j==0) return false;
		else if(i==7 && j==0) return false;
		else if(i==14 && j==2) return false;
		else if(i==14 && j==10) return false;

		/*新增针对于中间的六边形的*/
		else if(i==6 && j==10) return false;
		else if(i==7 && j==10) return false;
		else if(i==5 && j==9) return false;
		else if(i==6 && j==9) return false;
		else if(i==4 && j==8) return false;
		else if(i==5 && j==8) return false;
		else if(i==4 && j==7) return false;
		else if(i==5 && j==7) return false;
		else if(i==4 && j==6) return false;
		else if(i==5 && j==6) return false;

		else if(j==1 && (i==8 || i==9 || i==10 || i==11 || i==12 || i==13 || i==14)) return false;
		else if(j==0 && (i==9 || i==10 || i==11 || i==12 || i==13 || i==14)) return false;

		else {
			return true;
		}


	}

	/*过滤第一种棋子的第四种走法的不可用点*/
	private static boolean filter_0_4(int i, int j) {
		if(i==0 || i==1) return false;
		else if(i==9 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==2 && j==2) return false;
		else if(i==2 && j==10) return false;

		/*新增针对于中间的六边形的*/
		else if(i==10 && j==10) return false;
		else if(i==9 && j==10) return false;
		else if(i==11 && j==9) return false;
		else if(i==10 && j==9) return false;
		else if(i==12 && j==8) return false;
		else if(i==11 && j==8) return false;
		else if(i==12 && j==7) return false;
		else if(i==11 && j==7) return false;
		else if(i==12 && j==6) return false;
		else if(i==11 && j==6) return false;


		else if(j==1 && (i==2 || i==3 || i==4 || i==5 || i==6 || i==7 || i==8)) return false;
		else if(j==0 && (i==2 || i==3 || i==4 || i==5 || i==6 || i==7)) return false;

		else {
			return true;
		}
	}

	private static boolean filter_0_5(int i, int j) {

		if(i==0 || i==1) return false;
		else if(i==2 && j==8) return false;
		else if(i==2 && j==0) return false;
		else if(i==10 && j==14) return false;
		else if(i==9 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==7 && j==15) return false;
		else if(i==6 && j==14) return false;
		else if(i==5 && j==13) return false;
		else if(i==4 && j==12) return false;
		else if(i==3 && j==11) return false;
		else if(i==2 && j==10) return false;
		else if(i==7 && j==14) return false;
		else if(i==6 && j==13) return false;
		else if(i==5 && j==12) return false;
		else if(i==4 && j==11) return false;
		else if(i==3 && j==10) return false;
		else if(i==2 && j==9) return false;

		/*新增针对于中间的六边形的*/
		else if(i==9 && j==5) return false;
		else if(i==10 && j==4) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==4) return false;
		else if(i==11 && j==5) return false;
		else if(i==12 && j==4) return false;
		else if(i==11 && j==6) return false;
		else if(i==12 && j==5) return false;
		else if(i==11 && j==7) return false;
		else if(i==12 && j==6) return false;


		else {
			return true;
		}
	}

	private static boolean filter_0_6(int i, int j) {

		if(i==15 || i==16) return false;
		else if(i==14 && j==8) return false;
		else if(i==14 && j==0) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==9 && j==14) return false;
		else if(i==10 && j==13) return false;
		else if(i==11 && j==12) return false;
		else if(i==12 && j==11) return false;
		else if(i==13 && j==10) return false;
		else if(i==14 && j==9) return false;

		/*新增针对于中间的六边形的*/
		else if(i==7 && j==5) return false;
		else if(i==6 && j==4) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==4) return false;
		else if(i==5 && j==5) return false;
		else if(i==4 && j==4) return false;
		else if(i==5 && j==6) return false;
		else if(i==4 && j==5) return false;
		else if(i==5 && j==7) return false;
		else if(i==4 && j==6) return false;

		else {
			return true;
		}
	}


	private static boolean filter_1_0(int i, int j,int level){
		if(j==0 || j==ChessBoardModel.chessBoard.chessPoints[level][i].length-1) return false;
		if(i==8 && (j==1 || j==15))	return false;

	    if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==10 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;



		else if(i==0 && j==7) return false;
		else if(i==0 && j==1) return false;
		else if(i==16 && j==7) return false;
		else if(i==16 && j==1) return false;
		else if(i==8 && j==5) return false;


		return true;
	}

	private static boolean filter_1_1(int i, int j){
		if(i==0 || i==16) return false;
		if(i==8 && (j==0|| j==16))return false;
		if(i==1 && j==9) return false;
		if(i==7 && j==15) return false;
		if(i==9 && j==15) return false;
		if(i==10 && j==14) return false;
		if(i==11 && j==13) return false;
		if(i==12 && j==12) return false;
		if(i==13 && j==11) return false;
		if(i==14 && j==10) return false;
		if(i==15 && j==9) return false;
		if(i==15 && j==0) return false;
		if(i==9 && j==0) return false;
		if(i==7 && j==0) return false;
		if(i==6 && j==0) return false;
		if(i==5 && j==0) return false;
		if(i==4 && j==0) return false;
		if(i==3 && j==0) return false;
		if(i==2 && j==0) return false;
		if(i==1 && j==0) return false;
		if(i==7 && j==5) return false;
		if(i==6 && j==5) return false;
		if(i==5 && j==5) return false;
		if(i==5 && j==6) return false;
		if(i==5 && j==7) return false;

		else if(i==0 && j==7) return false;
		else if(i==0 && j==1) return false;
		else if(i==16 && j==7) return false;
		else if(i==16 && j==1) return false;
		else if(i==16 && j==2) return false;

		/*六边形*/
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;

		return true;
	}

	private static boolean filter_1_2(int i, int j){
		if(i==0 || i==16) return false;
		if(i==8 && (j==0|| j==16))return false;

		if(i==1 && j==9) return false;
		if(i==1 && j==0) return false;
		if(i==2 && j==10) return false;
		if(i==3 && j==11) return false;
		if(i==4 && j==12) return false;
		if(i==5 && j==13) return false;
		if(i==6 && j==14) return false;
		if(i==7 && j==15) return false;
		if(i==7 && j==0) return false;
		if(i==9 && j==15) return false;
		if(i==9 && j==0) return false;
		if(i==10 && j==0) return false;
		if(i==11 && j==0) return false;
		if(i==12 && j==0) return false;
		if(i==13 && j==0) return false;
		if(i==14 && j==0) return false;
		if(i==15 && j==0) return false;
		if(i==15 && j==9) return false;

		else if(i==0 && j==7) return false;
		else if(i==0 && j==1) return false;
		else if(i==16 && j==7) return false;
		else if(i==16 && j==1) return false;
		else if(i==16 && j==2) return false;

		if(i==9 && j==5) return false;
		if(i==10 && j==5) return false;
		if(i==11 && j==5) return false;
		if(i==11 && j==6) return false;
		if(i==11 && j==7) return false;

		/**/
		else if(i==7 && j==10) return false;
		else if(i==6 && j==9) return false;
		else if(i==5 && j==8) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==6) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;

		return true;
	}

	private static boolean filter_2_0(int i, int j){
		if(i==0 || j==0) return false;
		else if(i==1 && j==1) return false;
		else if(i==1 && j==9) return false;
		else if(i==8 && j==1) return false;
		else if(i==16 && j==1) return false;

		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;

		return true;
		}

	private static boolean filter_2_1(int i, int j){
		if(i==0) return false;
		else if(i==1 && j==9) return false;
		else if(i==1 && j==8) return false;
		else if(i==1 && j==1) return false;
		else if(i==1 && j==0) return false;
		else if(i==2 && j==0) return false;
		else if(i==3 && j==0) return false;
		else if(i==4 && j==0) return false;
		else if(i==5 && j==0) return false;
		else if(i==6 && j==0) return false;
		else if(i==7 && j==0) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==9 && j==0) return false;
		else if(i==9 && j==15) return false;

		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==9 && j==5) return false;

		return true;
	}

	private static boolean filter_2_2(int i, int j){
		if(i==0)	return false;
		else if(i==1 && j==0) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==9 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==11 && j==0) return false;
		else if(i==12 && j==0) return false;
		else if(i==13 && j==0) return false;
		else if(i==14 && j==0) return false;
		else if(i==15 && j==0) return false;
		else if(i==16 && j==7) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;
		else if(i==7 && j==5) return false;
		return true;

	}
	private static boolean filter_2_3(int i, int j){
		//System.out.println(i+" "+j);
		if(i==16) return false;
		else if(i==0 && j==7) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==0) return false;

		else if(i==5 && j==7) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;

		return true;

	}
	private static boolean filter_2_4(int i, int j){
		if(i==16) return false;
		else if(i==7 && j==15) return false;
		else if(i==7 && j==0) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==8) return false;
		else if(i==15 && j==1) return false;
		else if(i==15 && j==0) return false;
		else if(i==14 && j==0) return false;
		else if(i==13 && j==0) return false;
		else if(i==12 && j==0) return false;
		else if(i==11 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==9 && j==0) return false;
		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;

		return true;

	}
	private static boolean filter_2_5(int i, int j){

		if(i==16 || j==0) return false;
		else if(i==0 && j==1) return false;
		else if(i==15 && j==1) return false;
		else if(i==8 && j==1) return false;
		else if(i==15 && j==9) return false;

		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==5 && j==8) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==6) return false;
		return true;

	}

	private static boolean filter_3_0(int i, int j){
		if(i==0 || (i<=8 && j==1) ||j==0) return false;
		else if(i==1 && j==9) return false;
		else if(i==1 && j==2) return false;
		else if(i==11 && j==8) return false;
		else if(i==10 && j==9) return false;
		else if(i==9 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==7 && j==10) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		/**/
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		else if(i==9 && j==11) return false;
		else if(i==10 && j==10) return false;
		else if(i==11 && j==9) return false;


		return true;
	}
	private static boolean filter_3_1(int i, int j){
		if(i==0 || i==1) return false;
		else if(i==2 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==2 && j==1) return false;
		else if(i==2 && j==0) return false;
		else if(i==3 && j==0) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==4 && j==0) return false;
		else if(i==5 && j==0) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==6 && j==0) return false;
		else if(i==7 && j==0) return false;
		else if(i==7 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==9) return false;


		/**/
		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==8) return false;
		else if(i==10 && j==9) return false;
		else if(i==12 && j==5) return false;
		else if(i==12 && j==6) return false;
		else if(i==12 && j==7) return false;

		return true;
	}

	private static boolean filter_3_2(int i, int j){
		if(i==0) return false;
		else if(i==1 && j==9) return false;
		else if(i==1 && j==8) return false;
		else if(i==1 && j==7) return false;
		else if(i==2 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==3 && j==10) return false;
		else if(i==4 && j==12) return false;
		else if(i==4 && j==11) return false;
		else if(i==5 && j==13) return false;
		else if(i==5 && j==12) return false;
		else if(i==6 && j==14) return false;
		else if(i==6 && j==13) return false;
		else if(i==7 && j==15) return false;
		else if(i==7 && j==14) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==9 && j==14) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==16 && j==7) return false;

		else if(i==6 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==10 && j==4) return false;
		else if(i==9 && j==4) return false;
		else if(i==11 && j==4) return false;


		return true;
	}
	private static boolean filter_3_3(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==16) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==7 && j==14) return false;
		else if(i==9 && j==15) return false;
		else if(i==9 && j==14) return false;
		else if(i==10 && j==14) return false;
		else if(i==10 && j==13) return false;
		else if(i==11 && j==13) return false;
		else if(i==11 && j==12) return false;
		else if(i==12 && j==12) return false;
		else if(i==12 && j==11) return false;
		else if(i==13 && j==11) return false;
		else if(i==13 && j==10) return false;
		else if(i==14 && j==10) return false;
		else if(i==14 && j==9) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==8) return false;
		else if(i==15 && j==0) return false;
		else if(i==5 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;

		else if(i==8 && j==15) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;

		else if(i==7 && j==4) return false;
		else if(i==6 && j==4) return false;
		else if(i==5 && j==4) return false;
		return true;
	}
	private static boolean filter_3_4(int i, int j){

		if(i==15 || i==16)	return false;
		else if(i==7 && j==0) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==14 && j==9) return false;
		else if(i==15 && j==8) return false;
		else if(i==15 && j==0) return false;
		else if(i==14 && j==0) return false;
		else if(i==13 && j==0) return false;
		else if(i==12 && j==0) return false;
		else if(i==11 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==9 && j==0) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;

		else if(i==4 && j==5) return false;
		else if(i==4 && j==6) return false;
		else if(i==4 && j==7) return false;

		return true;


	}
	private static boolean filter_3_5(int i, int j){
		//System.out.println(i+" " +j);
		if(j==0) return false;
		else if(i==0 && j==1) return false;
		else if(i==7 && j==1) return false;
		else if(i==9 && j==1) return false;
		else if(i==10 && j==1) return false;
		else if(i==11 && j==1) return false;
		else if(i==12 && j==1) return false;
		else if(i==13 && j==1) return false;
		else if(i==14 && j==1) return false;
		else if(i==15 && j==1) return false;
		else if(i==16 && j==1) return false;
		else if(i==15 && j==2) return false;
		else if(i==16 && j==2) return false;
		else if(i==16 && j==3) return false;
		else if(i==16 && j==4) return false;
		else if(i==16 && j==5) return false;
		else if(i==16 && j==6) return false;
		else if(i==16 && j==7) return false;
		else if(i==8 && j==1) return false;

		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==5 && j==8) return false;
		else if(i==5 && j==7) return false;


		else if(i==7 && j==11) return false;
		else if(i==6 && j==10) return false;
		else if(i==5 && j==9) return false;
		return true;
	}

	private static boolean filter_4_0(int i, int j){
		if(i==0 || j==0 || i==16) return false;
		else if(i==1 && j==9) return false;
		else if(i==1 && j==1) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==1) return false;

		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;


		return true;
	}
	private static boolean filter_4_1(int i, int j){
		if(i==0 || j==0) return false;
		else if(i==1 && j==8) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==1) return false;
		else if(i==9 && j==15) return false;
		else if(i==16 && j==1) return false;

		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;
		else if(i==10 && j==9) return false;
		else if(i==9 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==7 && j==10) return false;
		else if(i==6 && j==9) return false;
		else if(i==10 && j==5) return false;
		else if(i==9 && j==5) return false;

		return true;
	}

	private static boolean filter_4_2(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==0) return false;

		else if(i==1 && j==0) return false;
		else if(i==1 && j==1) return false;
		else if(i==2 && j==0) return false;
		else if(i==3 && j==0) return false;
		else if(i==4 && j==0) return false;
		else if(i==5 && j==0) return false;
		else if(i==6 && j==0) return false;
		else if(i==7 && j==0) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==0) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==16 && j==7) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;

		else if(i==6 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==8) return false;
		else if(i==10 && j==9) return false;
		else if(i==9 && j==10) return false;

		return true;
	}
	private static boolean filter_4_3(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==16 || i==0) return false;

		else if(i==1 && j==0) return false;
		else if(i==1 && j==1) return false;
		else if(i==1 && j==8) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==15 && j==0) return false;
		else if(i==15 && j==8) return false;
		else if(i==15 && j==9) return false;
		else if(i==14 && j==10) return false;
		else if(i==13 && j==11) return false;
		else if(i==12 && j==12) return false;
		else if(i==11 && j==13) return false;
		else if(i==10 && j==14) return false;
		else if(i==9 && j==15) return false;

		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==9 && j==5) return false;



		return true;
	}
	private static boolean filter_4_4(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==16)	return false;
		else if(i==1 && j==9) return false;
		else if(i==0 && j==7) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==1) return false;
		else if(i==15 && j==0) return false;
		else if(i==14 && j==0) return false;
		else if(i==13 && j==0) return false;
		else if(i==12 && j==0) return false;
		else if(i==11 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==9 && j==0) return false;
		else if(i==7 && j==0) return false;

		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;


		return true;


	}
	private static boolean filter_4_5(int i, int j){
		//System.out.println(i+" " +j);
		if(j==0 || i==16) return false;
		else if(i==0 && j==1) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==1) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==8) return false;

		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;

		return true;
	}

	private static boolean filter_5_0(int i, int j){
		if(i==0 || j==0) return false;
		else if(i==1 && j==1) return false;
		else if(i==2 && j==1) return false;
		else if(i==3 && j==1) return false;
		else if(i==4 && j==1) return false;
		else if(i==5 && j==1) return false;
		else if(i==6 && j==1) return false;
		else if(i==7 && j==1) return false;
		else if(i==8 && j==1) return false;
		else if(i==9 && j==1) return false;
		else if(i==16 && j==1) return false;

		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==10 && j==9) return false;
		else if(i==11 && j==8) return false;
		else if(i==11 && j==7) return false;

		else if(i==7 && j==4) return false;
		else if(i==6 && j==4) return false;
		else if(i==5 && j==4) return false;


		return true;
	}
	private static boolean filter_5_1(int i, int j){
		if(i==0 || i==1) return false;
		else if(i==2 && j==10) return false;
		else if(i==2 && j==9) return false;
		else if(i==2 && j==1) return false;
		else if(i==2 && j==0) return false;
		else if(i==3 && j==0) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==4 && j==0) return false;
		else if(i==5 && j==0) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==6 && j==0) return false;
		else if(i==7 && j==0) return false;
		else if(i==7 && j==15) return false;
		else if(i==9 && j==0) return false;

		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;
		else if(i==11 && j==8) return false;
		else if(i==10 && j==9) return false;
		else if(i==9 && j==10) return false;

		else if(i==4 && j==7) return false;
		else if(i==4 && j==6) return false;
		else if(i==4 && j==5) return false;


		return true;
	}

	private static boolean filter_5_2(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==0) return false;

		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==1 && j==8) return false;
		else if(i==2 && j==9) return false;
		else if(i==3 && j==10) return false;
		else if(i==4 && j==11) return false;
		else if(i==5 && j==12) return false;
		else if(i==6 && j==13) return false;
		else if(i==7 && j==14) return false;
		else if(i==8 && j==15) return false;
		else if(i==1 && j==7) return false;
		else if(i==1 && j==0) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==15 && j==9) return false;
		else if(i==9 && j==14) return false;
		else if(i==9 && j==14) return false;

		else if(i==8 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;
		else if(i==11 && j==5) return false;
		else if(i==11 && j==6) return false;
		else if(i==11 && j==7) return false;

		else if(i==7 && j==11) return false;
		else if(i==6 && j==10) return false;
		else if(i==5 && j==9) return false;



		return true;
	}
	private static boolean filter_5_3(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==16) return false;
		else if(i==0 && j==7) return false;
		else if(i==1 && j==9) return false;
		else if(i==2 && j==10) return false;
		else if(i==3 && j==11) return false;
		else if(i==4 && j==12) return false;
		else if(i==5 && j==13) return false;
		else if(i==6 && j==14) return false;
		else if(i==7 && j==15) return false;
		else if(i==8 && j==15) return false;
		else if(i==9 && j==15) return false;
		else if(i==9 && j==14) return false;
		else if(i==10 && j==14) return false;
		else if(i==10 && j==13) return false;
		else if(i==11 && j==13) return false;
		else if(i==11 && j==12) return false;
		else if(i==12 && j==12) return false;
		else if(i==12 && j==11) return false;
		else if(i==13 && j==11) return false;
		else if(i==13 && j==10) return false;
		else if(i==14 && j==10) return false;
		else if(i==14 && j==9) return false;
		else if(i==15 && j==9) return false;
		else if(i==15 && j==8) return false;
		else if(i==8 && j==15) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==7 && j==5) return false;
		else if(i==8 && j==5) return false;
		else if(i==9 && j==5) return false;
		else if(i==10 && j==5) return false;


		else if(i==9 && j==11) return false;
		else if(i==10 && j==10) return false;
		else if(i==11 && j==9) return false;



		return true;
	}
	private static boolean filter_5_4(int i, int j){
		//System.out.println(i+"  "+j);
		if(i==16 || i==15)	return false;

		else if(i==7 && j==15) return false;
		else if(i==9 && j==0) return false;
		else if(i==10 && j==0) return false;
		else if(i==11 && j==0) return false;
		else if(i==12 && j==0) return false;
		else if(i==13 && j==0) return false;
		else if(i==14 && j==0) return false;
		else if(i==15 && j==0) return false;
		else if(i==14 && j==1) return false;
		else if(i==15 && j==1) return false;
		else if(i==9 && j==15) return false;
		else if(i==10 && j==14) return false;
		else if(i==11 && j==13) return false;
		else if(i==12 && j==12) return false;
		else if(i==13 && j==11) return false;
		else if(i==14 && j==10) return false;
		else if(i==7 && j==5) return false;
		else if(i==6 && j==5) return false;
		else if(i==5 && j==5) return false;
		else if(i==5 && j==6) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==14 && j==9) return false;

		else if(i==12 && j==7) return false;
		else if(i==12 && j==6) return false;
		else if(i==12 && j==5) return false;


		return true;


	}
	private static boolean filter_5_5(int i, int j){
		if(i==16 || j==0) return false;
		else if(i==7 && j==1) return false;
		else if(i==8 && j==1) return false;
		else if(i==9 && j==1) return false;
		else if(i==10 && j==1) return false;
		else if(i==11 && j==1) return false;
		else if(i==12 && j==1) return false;
		else if(i==13 && j==1) return false;
		else if(i==14 && j==1) return false;
		else if(i==15 && j==1) return false;
		else if(i==15 && j==2) return false;
		else if(i==5 && j==8) return false;
		else if(i==6 && j==9) return false;
		else if(i==7 && j==10) return false;
		else if(i==8 && j==11) return false;
		else if(i==9 && j==10) return false;
		else if(i==5 && j==7) return false;
		else if(i==5 && j==6) return false;

		else if(i==11 && j==4) return false;
		else if(i==10 && j==4) return false;
		else if(i==9 && j==4) return false;


		return true;
	}


}
