package com.punct.move;

public class AppendChessImg {

	public AppendChessImg() {

	}

	public static String getChessImgURL(String chessID) {
		if (chessID.equals("pane_white_0"))
			return "piece_0_0.png";
		else if (chessID.equals("pane_white_1"))
			return "piece_0_1.png";
		else if (chessID.equals("pane_white_2"))
			return "piece_0_5.png";
		else if (chessID.equals("pane_white_3"))
			return "piece_0_2.png";
		else if (chessID.equals("pane_white_4"))
			return "piece_0_3.png";
		else if (chessID.equals("pane_white_5"))
			return "piece_0_4.png";
		if (chessID.equals("pane_black_0"))
			return "piece_1_0.png";
		else if (chessID.equals("pane_black_1"))
			return "piece_1_1.png";
		else if (chessID.equals("pane_black_2"))
			return "piece_1_5.png";
		else if (chessID.equals("pane_black_3"))
			return "piece_1_2.png";
		else if (chessID.equals("pane_black_4"))
			return "piece_1_3.png";
		else if (chessID.equals("pane_black_5"))
			return "piece_1_4.png";
		else {
			 return "";
		}
	}

	/*chessType是棋子的种类，按照面板上面的排序方法排0~5；
	 * PointX是落子点目前的坐标。因为贴图时，原图和落子点的大小有所误差
	 * dir是棋子应该要的方向。棋子是绕中心旋转的。所以要适当修改坐标
	 * 绕任意点旋转后的左边公式为：
	 *
	 * 对图片上任意点(x,y)，绕一个坐标点(rx0,ry0)逆时针旋转a角度后的新的坐标设为(x0, y0)，有公式：
	 * x0= (x - rx0)*cos(a) - (y - ry0)*sin(a) + rx0 ;
	 * y0= (x - rx0)*sin(a) + (y - ry0)*cos(a) + ry0 ;
	 * */
	public static int getChessLocationX(int chessType,int PointX,int PointY,int dir) {

		switch (chessType) {
		case 0:
			return PointX-40;
		case 1:
			return PointX-40;
		case 2:
			return PointX-60;
		case 3:
			return PointX-56;
		case 4:
			return PointX-25;
		case 5:
			return PointX-25;

		default:
			return 0;
		}

	}

	public static int getChessLocationY(int chessType,int PointX,int PointY,int dir) {

		switch (chessType) {
		case 0:
			return PointY-20;
		case 1:
			return PointY-55;
		case 2:
			return PointY-42;
		case 3:
			return PointY-17;
		case 4:
			return PointY-33;
		case 5:
			return PointY-68;

		default:
			return 0;
		}

	}

	public static double getSuitableRotateX(int chessType,double x)
	{
		switch (chessType) {
		case 0:
			return  x+50;
		case 1:
			return x+50;
		case 2:
			return x+70;
		case 3:
			return x+66;
		case 4:
			return x+35;
		case 5:
			return x+35;
		default:
			return 0;
		}

	}


	public static double getSuitableRotateY(int chessType,double y)
	{
		switch (chessType) {
		case 0:return y+30;
		case 1:
			return y+65;
		case 2:
			return y+51;
		case 3:
			return y+27;
		case 4:
			return y+43;
		case 5:
			return y+78;
		default:
			return 0;
		}

	}
}
