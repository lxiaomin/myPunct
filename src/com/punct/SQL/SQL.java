package com.punct.SQL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.punct.logic.MD5Util;

public class SQL {

	PreparedStatement ps = null;// 这个可以让数据库变得更加安全一些
	ResultSet rs = null;
	Statement sm = null;
	Connection conn = null;
	public SQL() {
		// TODO Auto-generated constructor stub
	}

	public void LoadDrive() throws SQLException { // 载入驱动
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn=DriverManager.getConnection(
			"jdbc:sqlserver://localhost:1433;databasename=PunctDB","sa","sa");
			sm = conn.createStatement(); // 第三个关于链接数据库的句子
			// 之前的用户sa登陆失败是因为我的sever的服务器名称不对，本机存在了几个服务器，链接的服务器是上面的那个127.0.0.1

		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
	}


	public long createBoardID(int username1,int username2,int typeID)
	{

		long id=System.currentTimeMillis();
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		try {
			String timenow = "";
			SimpleDateFormat SDF = new SimpleDateFormat("yyyy'-'MM'-'dd' 'HH:mm:ss"); // 设置建立时间的格式
			Calendar currentTime = Calendar.getInstance();
			timenow = SDF.format(currentTime.getTime()); // 获得指定格式的系统时间（本地）

			String sql = "insert into eachBoardRecord(boardid,whitePlayerPointsChange,balckPlayerPointsChange,begintime,typeid)values("+id+",'" + username1+"','" + username2+"','"+timenow+"',3)";

			sm.executeUpdate(sql);// 执行更新语句
			return id;
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}
		return id;

	}

	public boolean addBoardState(String str,int step,long id) // 登陆验证
	{

		boolean flag = false;
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		try {
			String sql = "insert into eachSteps(boardid,steps,gamedata) values("+id+"," + step+",'" + str+"')";
			sm.executeUpdate(sql);// 执行更新语句
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return flag;

	}

	public String getBoardState(long id,int step)
	{
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		String state = "";
		try {
			String sql = "select gamedata from eachSteps where boardid='" + id + "' and steps ='"+step+"'" ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = rs.getString(1); // 从第三列取出
				}

			}

		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return state;
	}


	public void deleteStates(long boardID,int step) {

		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		try {
			String sql = "delete from eachsteps where boardid='" + boardID + "' and steps ='"+step+"'" ;
			sm.executeUpdate(sql);// 执行更新语句
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

	}

	public static void main(String[] args)  {
		SQL sqlConnection=new	SQL();
		try {
			sqlConnection.LoadDrive() ;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}

	public int LoginVerify(String uid, String psw) // 登陆验证
	{

		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		String SQLUserName = null;
		String SQLPassword = null; // 定义两个从SQL取出来的用户名和密码
		try {
			String sql = "select *from UserInfo where UserName='" + uid + "'";
			rs = sm.executeQuery(sql);// 取出这个用户,executeQuery()这个方法返回一个resultset的对象，说明rs已经保存了上面查询的结果

			if (!rs.equals(null)) {
				while (rs.next()) {
					SQLUserName = rs.getString("UserName");
					SQLPassword = rs.getString(3); // 从第三列取出
				}

				if (psw.equals(SQLPassword)) {
					return 2; // 2代表密码正确的状态，可以允许登陆。
				} else {
					return 1; // 1代表密码错误的状态，不可以登陆。
				}

			}

			else {
				return 0; // 0代表用户名不存在的状态，不可以登陆。
			}
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return 0;

	}

	public int AddUser(String uid, String psw, String email, String UserImage) // 添加用户
	{
		try {
			LoadDrive(); // 尝试加载驱动
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		int flag = 0;
		try {
			String timenow = "";
			SimpleDateFormat SDF = new SimpleDateFormat("yyyy'-'MM'-'dd' 'HH:mm:ss"); // 设置建立时间的格式
			Calendar currentTime = Calendar.getInstance();
			timenow = SDF.format(currentTime.getTime()); // 获得指定格式的系统时间（本地）

			MD5Util MD5psw = new MD5Util();
			String md5psw = MD5Util.MD5(psw); // md5算法加密后的密码

			String sql = "insert into [UserInfo] (UserName,userPsw,Email,IMAGEPATH,registerTime)values('" + uid
					+ "','" + md5psw + "','" + email + "','" + UserImage + "',+'" + timenow + "')";

			flag = sm.executeUpdate(sql);// 执行更新语句

		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return flag;

	}

	public int getUserID(String text) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		int state = 0;
		try {
			String sql = "select id from userinfo where username='" + text + "'" ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = Integer.parseInt(rs.getString(1)); // 从第三列取出
				}

			}

		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return state;

	}

	public void upIPToServer(String ip,String port,String name,String roomname) {
		boolean flag = false;
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		try {
			String sql = "insert into netmodeSearch(player1ip,PLAYER1PORT,player1name,roomname,STATE) values('"+ip+"','" + port+"','" + name+"','" + roomname+"',1)";
			sm.executeUpdate(sql);// 执行更新语句
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return ;

	}

	public String GetRooms() {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		String state = "";
		try {
			String sql = "select * from netmodeSearch where state!='3'" ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state+= rs.getString(2)+";"; // 从第三列取出
					state+= rs.getString(3)+";";
					state+= rs.getString(4)+";";
					state+= rs.getString(5)+";";
					state+= rs.getString(6)+";"; // 从第三列取出
					state+= rs.getString(7)+";";
					state+= rs.getString(8)+"*";
				}

			}

		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return state;
	}

<<<<<<< HEAD
	public int getNewStep(long id) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		int state = -1;
		try {
			String sql = "select MAX(STEPS) as step from eachSteps where boardID="+id+"" ;
			rs = sm.executeQuery(sql);

			while(rs.next())
			{
				state=rs.getInt(1);
			}

		} catch (Exception e) {
			return -1;
		}

		return state;
	}

	public String getMasterIP(String master) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		String state = "";
		try {
			String sql = "select PLAYER1IP,Roomname from netmodeSearch where PLAYER1NAME='"+master+"' and state='1'"  ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = rs.getString(1)+";";
					state += rs.getString(2);
				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		return state;
	}


//	public void UpdataRoom(String ip,String name,String masterIP,String roomID) {
//		boolean flag = false;
//		try {
//			LoadDrive();
//		} catch (SQLException e1) {
//
//			e1.printStackTrace();
//		}
//		try {
//			String sql = "update netmodeSearch(player2ip,player2name,STATE) values('"+ip+"','" + name+"',2) where PLAYER1IP='"+masterIP+"' AND STATE='1'";
//			sm.executeUpdate(sql);// 执行更新语句
//		} catch (Exception e) {
//			System.out.print(e);// 警告框弹出;
//		}
//
//		return ;
//
//	}

	public void UpdateRoomdata(int color, String playerName, long boardID, String roomname) {
		boolean flag = false;
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		String sql ;
		if(color==1)  sql = "update eachBoardRecord set whitePlayerID='"+playerName+"',roomname='"+roomname+"'  where boardID="+boardID+"";
		else sql = "update eachBoardRecord set blackPlayerID='"+playerName+"',roomname='"+roomname+"'  where boardID="+boardID+"";
		try {

			sm.executeUpdate(sql);// 执行更新语句
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}

		return ;

	}

	public int getcolor(long roomID) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		String state = "";
		try {
			String sql = "select whiteplayerID,blackplayerid from eachBoardRecord where boardid='"+roomID+"'"  ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = rs.getString(1);
					if(state!=null) return -1;
					state = rs.getString(2);
					if(state!=null) return 1;
				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}



		return 0;
	}

	public long getRoomID(String string) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		long state = 0;
		try {
			String sql = "select BoardID  from eachBoardRecord where Roomname='"+string+"'"  ;
			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = rs.getLong(1);

				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
			return state;
	}

	public String getOtherPlayerName(int quanxian, String roomname) {

		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		String sql ="";
		String state = null;
		if(quanxian==2) sql = "select PLAYER1NAME  from netmodeSearch where Roomname='"+roomname+"'"  ;
		if(quanxian==1) sql = "select PLAYER2NAME  from netmodeSearch where Roomname='"+roomname+"'"  ;
		try {

			rs = sm.executeQuery(sql);

			if (!rs.equals(null)) {
				while (rs.next()) {
					state = rs.getString(1);

				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
			return state;
	}



	public void UpdataRoom(String myip, String myname, String roomname,int color) {
		try {
			LoadDrive();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		String sql;
		sql= "update netmodeSearch set Player2NAME='"+myname+"',Player1ip='"+myip+"'  where roomname='"+roomname+"'";

		try {

			sm.executeUpdate(sql);// 执行更新语句
		} catch (Exception e) {
			System.out.print(e);// 警告框弹出;
		}



	}



=======
>>>>>>> origin/鏇存敼鐜鍚庣殑鍒嗘敮

}
