package com.punct.ModelDetective;

import java.util.Scanner;
import java.util.Stack;

public class Maze {
	public  static Stack<tagStep> PathStack=new Stack<tagStep>();
	public static int width;
	public static int height;

	static int maze[][]=new int[19][19];
	static int countStep=0;
	static char dirMazePath[][]=new char[19][19];
	static Stack<Integer> DirStack=new Stack<Integer>();
	private static int Edge;
	public static int startX=-1;
	public static int startY=-1;
	public static void readChessBoard(int[][] array)
	{
		Maze.width=Maze.height=array.length;
		for(int i=1;i<height+1;i++)
			for(int j =1;j<width+1;j++)
			{
				maze[i][j]=array[i-1][j-1];;
				dirMazePath[i][j]=(char) (maze[i][j]+48);
			}
		for(int i=0;i<width+2;i++)
		{
			maze[i][0]=1;
			maze[i][height+1]=1;
			dirMazePath[i][0]='1';
			dirMazePath[i][height+1]='1';
		}
		for(int i=0;i<height+2;i++)
		{
			maze[0][i]=1;
			maze[width+1][i]=1;
			dirMazePath[0][i]='1';
			dirMazePath[width+1][i]='1';
		}
//
//		for(int i=0;i<height+2;i++){
//			for(int j =0;j<width+2;j++)
//			{
//				System.out.print(maze[i][j]+" ");
//
//			}
//			System.out.println("");
//		}

	}


	static void printPath()
	{
		Stack<tagStep> tempStack=new Stack<>();
		while(!PathStack.empty())
		{
			tempStack.push(PathStack.peek());
			PathStack.pop();
		}
		WinLine.pathString="";//清空
		while(!tempStack.empty())
		{
			tagStep tempStep=tempStack.peek();
			if(tempStack.size()>1)
			{
				System.out.print("("+tempStep.x+","+tempStep.y+")"+"→");
				WinLine.pathString+=(tempStep.x-1)+";";
				WinLine.pathString+=(tempStep.y-1)+";";
				PathStack.push(tempStack.peek());
				tempStack.pop();
			}
			else
			{
				System.out.print("("+tempStep.x+","+tempStep.y+")");
				WinLine.pathString+=(tempStep.x-1)+";";
				WinLine.pathString+=(tempStep.y-1)+";";
				PathStack.push(tempStack.peek());
				tempStack.pop();
			}
		}
	}


	static void printMaze()
	{
		Stack<tagStep> tempStack=PathStack;
		while(!tempStack.empty())
		{
		    tagStep tempStep=tempStack.peek();
			tempStack.pop();
			int x=tempStep.x;
			int y=tempStep.y;
			maze[tempStep.x][tempStep.y]=219;
			dirMazePath[tempStep.x][tempStep.y]='@';
		}
//		for (int i=0;i<height+2;i++)
//		{
//			for (int j=0;j<width+2;j++)
//			{
//					System.out.print(dirMazePath[i][j]+"   ");
//			}
//			System.out.println("");
//		}


	}


	protected static int findPath()
	{
		PathStack.clear();
		tagStep start = new tagStep();
		start.x=startX;
		start.y=startY;
		start.diretion=1;
		tagStep currentPos=new tagStep();
		currentPos=start;
		PathStack.push(new tagStep(currentPos.x,currentPos.y,currentPos.diretion));
		while(!PathStack.empty())
		{
			/*如果可以访问的话*/
			if (Pass(currentPos))
			{
				    /*标记已经访问了*/
					Mark(currentPos);
					//currentPos.diretion=1;
					/*将点的方向压入方向栈中*/
					DirStack.push(currentPos.diretion);
					/*步数+1，可以通过栈的长度来判断要走多少步，这个步数统计用于寻找路径走了多少步*/
					countStep++;

					/*如果找到路径的话。输出矩阵和路径*/
					if(endPoint(currentPos.x,currentPos.y))
					{

						printPath();
						//System.out.println("find");
						//printMaze();
						return 1;
					}
					else
					{
						/*获取下一个点，从左上角开始*/
						currentPos=NextPos(currentPos,currentPos.diretion);
						PathStack.push(new tagStep(currentPos.x,currentPos.y,currentPos.diretion));
					}
			}
			else
			{

				if(!PathStack.empty())
				{

					/*把刚刚那个点去掉，回到上一个点*/
					PathStack.pop();
					currentPos=PathStack.peek();
					/*把刚刚那个店的方向去掉，回到上一个点的初始方向*/
					currentPos.diretion=DirStack.peek();
					currentPos.diretion++;
					DirStack.pop();
					DirStack.push(currentPos.diretion);
					countStep--;
					/*当八个方向都扫了一遍之后还是没有找到能走的点，则再回退，直到能有个点的8个方向没有走完为止*/
					while(currentPos.diretion>8 && !PathStack.empty())
					{
						Marked(currentPos);
						PathStack.pop();
						if(!PathStack.empty())
						{
						currentPos=PathStack.peek();
						DirStack.pop();
						currentPos.diretion=DirStack.peek();
						currentPos.diretion++;
						DirStack.pop();
						DirStack.push(currentPos.diretion);
						}
						else break;
					}

					if(currentPos.diretion<=8)
					{
						/*也是获取下一个点*/
						currentPos=NextPos(currentPos,DirStack.peek());
						/*把没走过的点初始化方向为左上角*/
						if(!IsPass(currentPos))
							currentPos.diretion=1;
						PathStack.push(new tagStep(currentPos.x,currentPos.y,currentPos.diretion));

					}
			}

		}
		}
		return 0;
	}

	/*判断该点是否被访问过*/
	private static boolean IsPass(tagStep currentPos)
	{
		/*如果该点的值为2的话，说明被改写过了，因此已经被访问过*/
		if(maze[currentPos.x][currentPos.y]==2)
			return true;
		return false;
	}

	/*判断该点是否可以通过*/
	private static boolean Pass(tagStep currentPos)
	{
		if(maze[currentPos.x][currentPos.y]==0)
			return true;
		return false;
	}

	/*访问该点，留下标记*/
	static void Mark(tagStep currentPos)
	{
		maze[currentPos.x][currentPos.y]=2;
	}

	static void Marked(tagStep currentPos)
	{
		maze[currentPos.x][currentPos.y]=2;
	}

	/*返回下一个点*/
	static tagStep NextPos(tagStep curPos,int pos)
	{
		tagStep NEWTAG=new tagStep();
		switch (pos)
		{

		case 1:NEWTAG.x=curPos.x;NEWTAG.y=curPos.y+1;break;

		case 2:NEWTAG.x=curPos.x+1;NEWTAG.y=curPos.y;break;

		case 3:NEWTAG.x=curPos.x;NEWTAG.y=curPos.y-1;break;

		case 4:NEWTAG.x=curPos.x-1;NEWTAG.y=curPos.y;break;
		case 5:NEWTAG.x=curPos.x-1;NEWTAG.y=curPos.y+1;break;
		case 6:NEWTAG.x=curPos.x+1;NEWTAG.y=curPos.y+1;break;
		case 7:NEWTAG.x=curPos.x+1;NEWTAG.y=curPos.y-1;break;
		case 8:NEWTAG.x=curPos.x-1;NEWTAG.y=curPos.y-1;break;
		default:
			break;
		}
		return NEWTAG;
	}



	public static int getEdge() {
		return Edge;
	}


	public void setEdge(int edge) {
		Edge = edge;
	}

	public static  boolean endPoint(int x,int y)
	{
		x=x-1;y=y-1;
		switch (getEdge()) {
		case 0:
			if(x==15 && y==9) return true;
			else if(x==14 && y==10) return true;
			else if(x==13 && y==11) return true;
			else if(x==12 && y==12) return true;
			else if(x==11 && y==13) return true;
			else if(x==10 && y==14) return true;
			else if(x==9 && y==15) return true;
			else return false;
		case 1:
		    if(x==16 && y==7) return true;
			else if(x==16 && y==6) return true;
			else if(x==16 && y==5) return true;
			else if(x==16 && y==4) return true;
			else if(x==16 && y==3) return true;
			else if(x==16 && y==2) return true;
			else if(x==16 && y==1) return true;
			else return false;

		case 2:
			if(x==9 && y==0) return true;
			else if(x==10 && y==0) return true;
			else if(x==11 && y==0) return true;
			else if(x==12 && y==0) return true;
			else if(x==13 && y==0) return true;
			else if(x==14 && y==0) return true;
			else if(x==15 && y==0) return true;
			else {
				return false;
			}
		case 3:
			if(x==1 && y==0) return true;
			else if(x==2 && y==0) return true;
			else if(x==3 && y==0) return true;
			else if(x==4 && y==0) return true;
			else if(x==5 && y==0) return true;
			else if(x==6 && y==0) return true;
			else if(x==7 && y==0) return true;
			else return false;

		case 4:
			if(x==0 && y==7) return true;
			else if(x==0 && y==6) return true;
			else if(x==0 && y==5) return true;
			else if(x==0 && y==4) return true;
			else if(x==0 && y==3) return true;
			else if(x==0 && y==2) return true;
			else if(x==0 && y==1) return true;
			else return false;

		case 5:
			if(x==1 && y==9) return true;
			else if(x==2 && y==10) return true;
			else if(x==3 && y==11) return true;
			else if(x==4 && y==12) return true;
			else if(x==5 && y==13) return true;
			else if(x==6 && y==14) return true;
			else if(x==7 && y==15) return true;
			else return false;

default:
	return false;
}
	}


//	public static void main(String[] args) {
//		Scanner sc=new Scanner(System.in);
//		int array[][]=new int[10][10];
//		for(int i=0;i<10;i++)
//			for(int j=0;j<10;j++)
//				array[i][j]=sc.nextInt();
//		readChessBoard(array);
//		findPath();
//	}
}
