package com.punct.ModelDetective;

import java.util.ArrayList;

import com.punct.controlers.ChessBoardModel;

public class WINJUDGE {
	private static ArrayList<String> WhitestartPoint=new ArrayList<String>();
	private static ArrayList<String> BlackstartPoint=new ArrayList<String>();
	public static int WhoWin()
	{
		int result=0;
		WhitestartPoint.clear();
		BlackstartPoint.clear();
		int whiteArray[][]=ChessBoardModel.getWhiteChessBoardModel();
		int blackArray[][]=ChessBoardModel.getBlackChessBoardModel();

		/*1、搜索六边形的六条边有没有可以走的点,有的话放到列表中，等下逐个从这些点出发寻找通路*/
		addStartPoints(whiteArray,1);
		addStartPoints(blackArray,-1);
		/*2、遍历列表，寻找可以作为起点的点，若没有则这次return，若有，则从这条路往下搜索*/
		if(WhitestartPoint.size()>0)
		{
			for(int i=0;i<WhitestartPoint.size();i++)
			{
				String startPoint=WhitestartPoint.get(i);
				result=InitMaze(startPoint,whiteArray,1);
				if(result!=0) return result;
			}

		}
		if(BlackstartPoint.size()>0) {
			for(int i=0;i<BlackstartPoint.size();i++)
			{
				String startPoint=BlackstartPoint.get(i);
				result=InitMaze(startPoint,blackArray,-1);
				if(result!=0) return result;
			}
		}
		return 0;

	}

	private static int InitMaze(String startPoint, int[][] array,int color) {

		String t[]=startPoint.split("_");
		int XYdata[]=new int [2];
		XYdata[0]=Integer.parseInt(t[0]);
		XYdata[1]=Integer.parseInt(t[1]);
		int StartEdge=WitchEdge(startPoint);
		Maze maze=new Maze();
		/*起点	*/
		maze.startX=XYdata[0]+1;
		maze.startY=XYdata[1]+1;
		maze.readChessBoard(array);
		maze.setEdge(StartEdge);
		int tempRe=maze.findPath();
		if(tempRe==1 && color==1)
			return 1;
		else 	if(tempRe==1 && color==-1)
			return -1;
		else {
			return 0;
		}


	}


	/*判断是从哪条边出发*/
	private static int WitchEdge(String startPoint) {
		if(startPoint.equals("7_0")) return 0;
		else if(startPoint.equals("6_0")) return 0;
		else if(startPoint.equals("5_0")) return 0;
		else if(startPoint.equals("4_0")) return 0;
		else if(startPoint.equals("3_0")) return 0;
		else if(startPoint.equals("2_0")) return 0;
		else if(startPoint.equals("1_0")) return 0;
		else if(startPoint.equals("0_1")) return 1;
		else if(startPoint.equals("0_2")) return 1;
		else if(startPoint.equals("0_3")) return 1;
		else if(startPoint.equals("0_4")) return 1;
		else if(startPoint.equals("0_6")) return 1;
		else if(startPoint.equals("0_7")) return 1;
		else if(startPoint.equals("0_5")) return 1;
		else if(startPoint.equals("1_9")) return 2;
		else if(startPoint.equals("2_10")) return 2;
		else if(startPoint.equals("3_11")) return 2;
		else if(startPoint.equals("4_12")) return 2;
		else if(startPoint.equals("5_13")) return 2;
		else if(startPoint.equals("6_14")) return 2;
		else if(startPoint.equals("7_15")) return 2;
		else if(startPoint.equals("9_15")) return 3;
		else if(startPoint.equals("10_14")) return 3;
		else if(startPoint.equals("11_13")) return 3;
		else if(startPoint.equals("12_12")) return 3;
		else if(startPoint.equals("13_11")) return 3;
		else if(startPoint.equals("14_10")) return 3;
		else if(startPoint.equals("15_9")) return 3;

		else if(startPoint.equals("16_7")) return 4;
		else if(startPoint.equals("16_6")) return 4;
		else if(startPoint.equals("16_5")) return 4;
		else if(startPoint.equals("16_4")) return 4;
		else if(startPoint.equals("16_3")) return 4;
		else if(startPoint.equals("16_2")) return 4;
		else if(startPoint.equals("16_1")) return 4;
		else if(startPoint.equals("15_0")) return 5;
		else if(startPoint.equals("14_0")) return 5;
		else if(startPoint.equals("13_0")) return 5;
		else if(startPoint.equals("12_0")) return 5;
		else if(startPoint.equals("11_0")) return 5;
		else if(startPoint.equals("10_0")) return 5;
		else if(startPoint.equals("9_0")) return 5;

		else return 0;
	}

	private static void addStartPoints(int[][] array,int color) {
		if(color==1)
		{
			if(array[1][9]==0) WhitestartPoint.add("1_9");
			else if(array[2][10]==0) WhitestartPoint.add("2_10");
			else if(array[3][11]==0) WhitestartPoint.add("3_11");
			else if(array[4][12]==0) WhitestartPoint.add("4_12");
			else if(array[5][13]==0) WhitestartPoint.add("5_13");
			else if(array[6][14]==0) WhitestartPoint.add("6_14");
			else if(array[7][15]==0) WhitestartPoint.add("7_15");
			else if(array[9][15]==0) WhitestartPoint.add("9_15");
			else if(array[10][14]==0) WhitestartPoint.add("10_14");
			else if(array[11][13]==0) WhitestartPoint.add("11_13");
			else if(array[12][12]==0) WhitestartPoint.add("12_12");
			else if(array[13][11]==0) WhitestartPoint.add("13_11");
			else if(array[14][10]==0) WhitestartPoint.add("14_10");
			else if(array[15][9]==0) WhitestartPoint.add("15_9");
			else if(array[16][7]==0) WhitestartPoint.add("16_7");
			else if(array[16][6]==0) WhitestartPoint.add("16_6");
			else if(array[16][5]==0) WhitestartPoint.add("16_5");
			else if(array[16][3]==0) WhitestartPoint.add("16_3");
			else if(array[16][2]==0) WhitestartPoint.add("16_2");
			else if(array[16][1]==0) WhitestartPoint.add("16_1");
			else if(array[15][0]==0) WhitestartPoint.add("15_0");
			else if(array[14][0]==0) WhitestartPoint.add("14_0");
			else if(array[13][0]==0) WhitestartPoint.add("13_0");
			else if(array[12][0]==0) WhitestartPoint.add("12_0");
			else if(array[11][0]==0) WhitestartPoint.add("11_0");
			else if(array[10][0]==0) WhitestartPoint.add("10_0");
			else if(array[9][0]==0) WhitestartPoint.add("9_0");
			else if(array[7][0]==0) WhitestartPoint.add("7_0");
			else if(array[6][0]==0) WhitestartPoint.add("6_0");
			else if(array[5][0]==0) WhitestartPoint.add("5_0");
			else if(array[4][0]==0) WhitestartPoint.add("4_0");
			else if(array[3][0]==0) WhitestartPoint.add("3_0");
			else if(array[2][0]==0) WhitestartPoint.add("2_0");
			else if(array[1][0]==0) WhitestartPoint.add("1_0");
			else if(array[0][1]==0) WhitestartPoint.add("0_1");
			else if(array[0][2]==0) WhitestartPoint.add("0_2");
			else if(array[0][3]==0) WhitestartPoint.add("0_3");
			else if(array[0][4]==0) WhitestartPoint.add("0_4");
			else if(array[0][5]==0) WhitestartPoint.add("0_5");
			else if(array[0][6]==0) WhitestartPoint.add("0_6");
			else if(array[0][7]==0) WhitestartPoint.add("0_7");
		}
		else
		{
			if(array[1][9]==0) BlackstartPoint.add("1_9");
			else if(array[2][10]==0) BlackstartPoint.add("2_10");
			else if(array[3][11]==0) BlackstartPoint.add("3_11");
			else if(array[4][12]==0) BlackstartPoint.add("4_12");
			else if(array[5][13]==0) BlackstartPoint.add("5_13");
			else if(array[6][14]==0) BlackstartPoint.add("6_14");
			else if(array[7][15]==0) BlackstartPoint.add("7_15");
			else if(array[9][15]==0) BlackstartPoint.add("9_15");
			else if(array[10][14]==0) BlackstartPoint.add("10_14");
			else if(array[11][13]==0) BlackstartPoint.add("11_13");
			else if(array[12][12]==0) BlackstartPoint.add("12_12");
			else if(array[13][11]==0) BlackstartPoint.add("13_11");
			else if(array[14][10]==0) BlackstartPoint.add("14_10");
			else if(array[15][9]==0) BlackstartPoint.add("15_9");
			else if(array[16][7]==0) BlackstartPoint.add("16_7");
			else if(array[16][6]==0) BlackstartPoint.add("16_6");
			else if(array[16][5]==0) BlackstartPoint.add("16_5");
			else if(array[16][3]==0) BlackstartPoint.add("16_3");
			else if(array[16][2]==0) BlackstartPoint.add("16_2");
			else if(array[16][1]==0) BlackstartPoint.add("16_1");
			else if(array[15][0]==0) BlackstartPoint.add("15_0");
			else if(array[14][0]==0) BlackstartPoint.add("14_0");
			else if(array[13][0]==0) BlackstartPoint.add("13_0");
			else if(array[12][0]==0) BlackstartPoint.add("12_0");
			else if(array[11][0]==0) BlackstartPoint.add("11_0");
			else if(array[10][0]==0) BlackstartPoint.add("10_0");
			else if(array[9][0]==0) BlackstartPoint.add("9_0");
			else if(array[7][0]==0) BlackstartPoint.add("7_0");
			else if(array[6][0]==0) BlackstartPoint.add("6_0");
			else if(array[5][0]==0) BlackstartPoint.add("5_0");
			else if(array[4][0]==0) BlackstartPoint.add("4_0");
			else if(array[3][0]==0) BlackstartPoint.add("3_0");
			else if(array[2][0]==0) BlackstartPoint.add("2_0");
			else if(array[1][0]==0) BlackstartPoint.add("1_0");
			else if(array[0][1]==0) BlackstartPoint.add("0_1");
			else if(array[0][2]==0) BlackstartPoint.add("0_2");
			else if(array[0][3]==0) BlackstartPoint.add("0_3");
			else if(array[0][4]==0) BlackstartPoint.add("0_4");
			else if(array[0][5]==0) BlackstartPoint.add("0_5");
			else if(array[0][6]==0) BlackstartPoint.add("0_6");
			else if(array[0][7]==0) BlackstartPoint.add("0_7");
		}

	}


}
